﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
namespace WindowsApplication1
{
    partial class Form_gis_road
    {

       
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true;否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            uninit_gis_road_basic_information_unit_dll();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

      void  uninit_gis_road_basic_information_unit_dll()
        {
          for(int i=0;i<gis_road_basic_information_unit_index.Length;i++)
              if (gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit != 0)
              {
                  uninit_road_set(ref gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit);
                  gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit=0;

              }
    }
    
        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_gis_road));
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.cms = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示基本信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.googleEarthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView_road_info = new System.Windows.Forms.TreeView();
            this.axMap1 = new AxMapXLib.AxMap();
            this.webBrowser_google_earth = new System.Windows.Forms.WebBrowser();
            this.treeListGis = new DevExpress.XtraTreeList.TreeList();
            this.name = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tuli = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.check = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.comboBoxYear = new System.Windows.Forms.ComboBox();
            this.checkBoxHigh = new System.Windows.Forms.CheckBox();
            this.checkBoxLow = new System.Windows.Forms.CheckBox();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripGoogleEarth = new System.Windows.Forms.ToolStripButton();
            this.button_indent = new System.Windows.Forms.Button();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.road_select_list = new DevExpress.XtraTreeList.TreeList();
            this.road_name = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.year = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.up_down_line = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.select = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.button_indent2 = new System.Windows.Forms.Button();
            this.google_earth = new System.Windows.Forms.Button();
            this.cms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axMap1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListGis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.road_select_list)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // openFile
            // 
            this.openFile.DefaultExt = "*.TAB";
            this.openFile.Filter = "MapInfo 表(*.tab)|*.tab";
            this.openFile.RestoreDirectory = true;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "\"windows bitmap(*.bmp)|*.bmp\" ";
            this.saveFileDialog.Title = "保存";
            // 
            // cms
            // 
            this.cms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示基本信息ToolStripMenuItem,
            this.toolStripSeparator1,
            this.googleEarthToolStripMenuItem});
            this.cms.Name = "cms";
            this.cms.Size = new System.Drawing.Size(143, 54);
            this.cms.Opening += new System.ComponentModel.CancelEventHandler(this.cms_Opening);
            // 
            // 显示基本信息ToolStripMenuItem
            // 
            this.显示基本信息ToolStripMenuItem.Name = "显示基本信息ToolStripMenuItem";
            this.显示基本信息ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.显示基本信息ToolStripMenuItem.Text = "显示基本信息";
            this.显示基本信息ToolStripMenuItem.Click += new System.EventHandler(this.显示基本信息ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
            // 
            // googleEarthToolStripMenuItem
            // 
            this.googleEarthToolStripMenuItem.Name = "googleEarthToolStripMenuItem";
            this.googleEarthToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.googleEarthToolStripMenuItem.Text = "google earth";
            this.googleEarthToolStripMenuItem.Click += new System.EventHandler(this.googleEarthToolStripMenuItem_Click);
            // 
            // treeView_road_info
            // 
            this.treeView_road_info.CheckBoxes = true;
            this.treeView_road_info.Location = new System.Drawing.Point(133, 75);
            this.treeView_road_info.Name = "treeView_road_info";
            this.treeView_road_info.Size = new System.Drawing.Size(212, 417);
            this.treeView_road_info.TabIndex = 13;
            this.treeView_road_info.Visible = false;
            this.treeView_road_info.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_road_info_NodeMouseDoubleClick);
            this.treeView_road_info.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView_road_info_AfterCheck);
            // 
            // axMap1
            // 
            this.axMap1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.axMap1.Enabled = true;
            this.axMap1.Location = new System.Drawing.Point(0, 254);
            this.axMap1.Name = "axMap1";
            this.axMap1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap1.OcxState")));
            this.axMap1.Size = new System.Drawing.Size(850, 854);
            this.axMap1.TabIndex = 12;
            this.axMap1.MapInitialized += new System.EventHandler(this.axMap1_MapInitialized);
            this.axMap1.MouseWheelEvent += new AxMapXLib.CMapXEvents_MouseWheelEventHandler(this.axMap1_MouseWheelEvent);
            this.axMap1.PolyToolUsed += new AxMapXLib.CMapXEvents_PolyToolUsedEventHandler(this.axMap1_PolyToolUsed);
            this.axMap1.MapViewChanged += new System.EventHandler(this.axMap1_MapViewChanged);
            this.axMap1.MouseUpEvent += new AxMapXLib.CMapXEvents_MouseUpEventHandler(this.axMap1_MouseUpEvent);
            // 
            // webBrowser_google_earth
            // 
            this.webBrowser_google_earth.Location = new System.Drawing.Point(409, 8);
            this.webBrowser_google_earth.MinimumSize = new System.Drawing.Size(20, 18);
            this.webBrowser_google_earth.Name = "webBrowser_google_earth";
            this.webBrowser_google_earth.Size = new System.Drawing.Size(291, 179);
            this.webBrowser_google_earth.TabIndex = 14;
            this.webBrowser_google_earth.Visible = false;
            // 
            // treeListGis
            // 
            this.treeListGis.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.treeListGis.Appearance.Empty.Options.UseBackColor = true;
            this.treeListGis.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.treeListGis.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.treeListGis.Appearance.EvenRow.Options.UseBackColor = true;
            this.treeListGis.Appearance.EvenRow.Options.UseForeColor = true;
            this.treeListGis.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.treeListGis.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.treeListGis.Appearance.FocusedCell.Options.UseBackColor = true;
            this.treeListGis.Appearance.FocusedCell.Options.UseForeColor = true;
            this.treeListGis.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.treeListGis.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.treeListGis.Appearance.FocusedRow.Options.UseBackColor = true;
            this.treeListGis.Appearance.FocusedRow.Options.UseForeColor = true;
            this.treeListGis.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.treeListGis.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.treeListGis.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.treeListGis.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.treeListGis.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.treeListGis.Appearance.FooterPanel.Options.UseBackColor = true;
            this.treeListGis.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.treeListGis.Appearance.FooterPanel.Options.UseForeColor = true;
            this.treeListGis.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.treeListGis.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.treeListGis.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.treeListGis.Appearance.GroupButton.Options.UseBackColor = true;
            this.treeListGis.Appearance.GroupButton.Options.UseBorderColor = true;
            this.treeListGis.Appearance.GroupButton.Options.UseForeColor = true;
            this.treeListGis.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.treeListGis.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.treeListGis.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.treeListGis.Appearance.GroupFooter.Options.UseBackColor = true;
            this.treeListGis.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.treeListGis.Appearance.GroupFooter.Options.UseForeColor = true;
            this.treeListGis.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.treeListGis.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.treeListGis.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.treeListGis.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.treeListGis.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.treeListGis.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.treeListGis.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.treeListGis.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.treeListGis.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.treeListGis.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.treeListGis.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.treeListGis.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.treeListGis.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.treeListGis.Appearance.HorzLine.Options.UseBackColor = true;
            this.treeListGis.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.treeListGis.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.treeListGis.Appearance.OddRow.Options.UseBackColor = true;
            this.treeListGis.Appearance.OddRow.Options.UseForeColor = true;
            this.treeListGis.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.treeListGis.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.treeListGis.Appearance.Preview.Options.UseBackColor = true;
            this.treeListGis.Appearance.Preview.Options.UseForeColor = true;
            this.treeListGis.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.treeListGis.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.treeListGis.Appearance.Row.Options.UseBackColor = true;
            this.treeListGis.Appearance.Row.Options.UseForeColor = true;
            this.treeListGis.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.treeListGis.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.treeListGis.Appearance.SelectedRow.Options.UseBackColor = true;
            this.treeListGis.Appearance.SelectedRow.Options.UseForeColor = true;
            this.treeListGis.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.treeListGis.Appearance.TreeLine.Options.UseBackColor = true;
            this.treeListGis.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.treeListGis.Appearance.VertLine.Options.UseBackColor = true;
            this.treeListGis.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.name,
            this.tuli,
            this.check});
            this.treeListGis.Location = new System.Drawing.Point(-9, 47);
            this.treeListGis.Name = "treeListGis";
            this.treeListGis.OptionsView.EnableAppearanceEvenRow = true;
            this.treeListGis.OptionsView.EnableAppearanceOddRow = true;
            this.treeListGis.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemImageEdit1,
            this.repositoryItemComboBox1});
            this.treeListGis.Size = new System.Drawing.Size(312, 445);
            this.treeListGis.TabIndex = 15;
            this.treeListGis.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.treeListGis_CustomDrawNodeCell);
            this.treeListGis.AfterCollapse += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListGis_AfterCollapse);
            this.treeListGis.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListGis_FocusedNodeChanged);
            this.treeListGis.CustomNodeCellEdit += new DevExpress.XtraTreeList.GetCustomNodeCellEditEventHandler(this.treeListGis_CustomNodeCellEdit);
            this.treeListGis.DoubleClick += new System.EventHandler(this.treeListGis_DoubleClick);
            this.treeListGis.Click += new System.EventHandler(this.treeListGis_Click);
            // 
            // name
            // 
            this.name.Caption = "名称";
            this.name.FieldName = "信息类型";
            this.name.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.name.MinWidth = 160;
            this.name.Name = "name";
            this.name.OptionsColumn.AllowEdit = false;
            this.name.Visible = true;
            this.name.VisibleIndex = 0;
            this.name.Width = 176;
            // 
            // tuli
            // 
            this.tuli.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.tuli.AppearanceCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.tuli.AppearanceCell.Options.UseForeColor = true;
            this.tuli.Caption = "图例";
            this.tuli.FieldName = "tuli";
            this.tuli.MinWidth = 40;
            this.tuli.Name = "tuli";
            this.tuli.OptionsColumn.AllowEdit = false;
            this.tuli.OptionsColumn.AllowFocus = false;
            this.tuli.Visible = true;
            this.tuli.VisibleIndex = 1;
            this.tuli.Width = 50;
            // 
            // check
            // 
            this.check.AppearanceHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.check.AppearanceHeader.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.check.AppearanceHeader.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.check.AppearanceHeader.Options.UseBackColor = true;
            this.check.AppearanceHeader.Options.UseBorderColor = true;
            this.check.Caption = "选择";
            this.check.FieldName = "check";
            this.check.MinWidth = 40;
            this.check.Name = "check";
            this.check.Visible = true;
            this.check.VisibleIndex = 2;
            this.check.Width = 50;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AllowFocused = false;
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // comboBoxYear
            // 
            this.comboBoxYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.comboBoxYear.FormattingEnabled = true;
            this.comboBoxYear.Location = new System.Drawing.Point(288, 111);
            this.comboBoxYear.Name = "comboBoxYear";
            this.comboBoxYear.Size = new System.Drawing.Size(150, 20);
            this.comboBoxYear.TabIndex = 16;
            this.comboBoxYear.Text = "2008";
            // 
            // checkBoxHigh
            // 
            this.checkBoxHigh.AutoSize = true;
            this.checkBoxHigh.Checked = true;
            this.checkBoxHigh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHigh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.checkBoxHigh.Location = new System.Drawing.Point(297, 171);
            this.checkBoxHigh.Name = "checkBoxHigh";
            this.checkBoxHigh.Size = new System.Drawing.Size(48, 16);
            this.checkBoxHigh.TabIndex = 17;
            this.checkBoxHigh.Text = "上行";
            this.checkBoxHigh.UseVisualStyleBackColor = true;
            this.checkBoxHigh.CheckedChanged += new System.EventHandler(this.checkBoxHigh_CheckedChanged);
            // 
            // checkBoxLow
            // 
            this.checkBoxLow.AutoSize = true;
            this.checkBoxLow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.checkBoxLow.Location = new System.Drawing.Point(374, 171);
            this.checkBoxLow.Name = "checkBoxLow";
            this.checkBoxLow.Size = new System.Drawing.Size(48, 16);
            this.checkBoxLow.TabIndex = 18;
            this.checkBoxLow.Text = "下行";
            this.checkBoxLow.UseVisualStyleBackColor = true;
            this.checkBoxLow.CheckedChanged += new System.EventHandler(this.checkBoxLow_CheckedChanged);
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(992, 429);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton14,
            this.toolStripButton11,
            this.toolStripButton10,
            this.toolStripButton8,
            this.toolStripButton6,
            this.toolStripGoogleEarth});
            this.toolStrip1.Location = new System.Drawing.Point(674, 9);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip1.Size = new System.Drawing.Size(300, 36);
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = global::WindowsApplication1.Properties.Resources.MC001;
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(28, 33);
            this.toolStripButton7.Text = "漫游";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::WindowsApplication1.Properties.Resources.MC002;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(28, 33);
            this.toolStripButton2.Text = "放大";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::WindowsApplication1.Properties.Resources.MC003;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(28, 33);
            this.toolStripButton3.Text = "缩小";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton14.Image = global::WindowsApplication1.Properties.Resources.MC019;
            this.toolStripButton14.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton14.Name = "toolStripButton14";
            this.toolStripButton14.Size = new System.Drawing.Size(28, 33);
            this.toolStripButton14.Text = "点选";
            this.toolStripButton14.Click += new System.EventHandler(this.toolStripButton14_Click);
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton11.Image = global::WindowsApplication1.Properties.Resources.MC021;
            this.toolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Size = new System.Drawing.Size(28, 33);
            this.toolStripButton11.Text = "框选";
            this.toolStripButton11.Click += new System.EventHandler(this.toolStripButton11_Click);
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton10.Image = global::WindowsApplication1.Properties.Resources.MC027;
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Margin = new System.Windows.Forms.Padding(4);
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton10.Text = "图层控制";
            this.toolStripButton10.Click += new System.EventHandler(this.toolStripButton10_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = global::WindowsApplication1.Properties.Resources.MC009;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(28, 33);
            this.toolStripButton8.Text = "标签";
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(28, 33);
            this.toolStripButton6.Text = "信息";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripGoogleEarth
            // 
            this.toolStripGoogleEarth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripGoogleEarth.Image = global::WindowsApplication1.Properties.Resources.earth;
            this.toolStripGoogleEarth.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGoogleEarth.Name = "toolStripGoogleEarth";
            this.toolStripGoogleEarth.Size = new System.Drawing.Size(28, 33);
            this.toolStripGoogleEarth.Text = "ｇｏｏｇｌｅ　ｅａｒｔｈ";
            this.toolStripGoogleEarth.Click += new System.EventHandler(this.toolStripGoogleEarth_Click);
            // 
            // button_indent
            // 
            this.button_indent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button_indent.BackgroundImage = global::WindowsApplication1.Properties.Resources.pixelicious_122;
            this.button_indent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_indent.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_indent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button_indent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_indent.Font = new System.Drawing.Font("Arial", 9F);
            this.button_indent.ForeColor = System.Drawing.Color.Transparent;
            this.button_indent.Location = new System.Drawing.Point(409, 208);
            this.button_indent.Name = "button_indent";
            this.button_indent.Size = new System.Drawing.Size(42, 25);
            this.button_indent.TabIndex = 19;
            this.button_indent.TabStop = false;
            this.button_indent.Text = " ";
            this.button_indent.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button_indent.UseVisualStyleBackColor = true;
            this.button_indent.Click += new System.EventHandler(this.button_indent_Click);
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // road_select_list
            // 
            this.road_select_list.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.road_select_list.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.road_select_list.Appearance.Empty.Options.UseBackColor = true;
            this.road_select_list.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.road_select_list.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.road_select_list.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.road_select_list.Appearance.EvenRow.Options.UseBackColor = true;
            this.road_select_list.Appearance.EvenRow.Options.UseBorderColor = true;
            this.road_select_list.Appearance.EvenRow.Options.UseForeColor = true;
            this.road_select_list.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.road_select_list.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.road_select_list.Appearance.FocusedCell.Options.UseBackColor = true;
            this.road_select_list.Appearance.FocusedCell.Options.UseForeColor = true;
            this.road_select_list.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.road_select_list.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.road_select_list.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.road_select_list.Appearance.FocusedRow.Options.UseBackColor = true;
            this.road_select_list.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.road_select_list.Appearance.FocusedRow.Options.UseForeColor = true;
            this.road_select_list.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.road_select_list.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.road_select_list.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.road_select_list.Appearance.FooterPanel.Options.UseBackColor = true;
            this.road_select_list.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.road_select_list.Appearance.FooterPanel.Options.UseForeColor = true;
            this.road_select_list.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.road_select_list.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.road_select_list.Appearance.GroupButton.Options.UseBackColor = true;
            this.road_select_list.Appearance.GroupButton.Options.UseBorderColor = true;
            this.road_select_list.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.road_select_list.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.road_select_list.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.road_select_list.Appearance.GroupFooter.Options.UseBackColor = true;
            this.road_select_list.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.road_select_list.Appearance.GroupFooter.Options.UseForeColor = true;
            this.road_select_list.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.road_select_list.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.road_select_list.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.road_select_list.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.road_select_list.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.road_select_list.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.road_select_list.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.road_select_list.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.road_select_list.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.road_select_list.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.road_select_list.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.road_select_list.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.road_select_list.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.road_select_list.Appearance.HorzLine.Options.UseBackColor = true;
            this.road_select_list.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.road_select_list.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.road_select_list.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.road_select_list.Appearance.OddRow.Options.UseBackColor = true;
            this.road_select_list.Appearance.OddRow.Options.UseBorderColor = true;
            this.road_select_list.Appearance.OddRow.Options.UseForeColor = true;
            this.road_select_list.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.road_select_list.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.road_select_list.Appearance.Preview.Options.UseFont = true;
            this.road_select_list.Appearance.Preview.Options.UseForeColor = true;
            this.road_select_list.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.road_select_list.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.road_select_list.Appearance.Row.Options.UseBackColor = true;
            this.road_select_list.Appearance.Row.Options.UseForeColor = true;
            this.road_select_list.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.road_select_list.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.road_select_list.Appearance.SelectedRow.Options.UseBackColor = true;
            this.road_select_list.Appearance.SelectedRow.Options.UseForeColor = true;
            this.road_select_list.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.road_select_list.Appearance.TreeLine.Options.UseBackColor = true;
            this.road_select_list.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.road_select_list.Appearance.VertLine.Options.UseBackColor = true;
            this.road_select_list.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.road_name,
            this.year,
            this.up_down_line,
            this.select});
            this.road_select_list.Location = new System.Drawing.Point(0, 12);
            this.road_select_list.Name = "road_select_list";
            this.road_select_list.OptionsView.EnableAppearanceEvenRow = true;
            this.road_select_list.OptionsView.EnableAppearanceOddRow = true;
            this.road_select_list.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEdit2});
            this.road_select_list.Size = new System.Drawing.Size(303, 203);
            this.road_select_list.TabIndex = 20;
            this.road_select_list.AfterCollapse += new DevExpress.XtraTreeList.NodeEventHandler(this.road_select_list_AfterCollapse);
            this.road_select_list.CustomNodeCellEdit += new DevExpress.XtraTreeList.GetCustomNodeCellEditEventHandler(this.road_select_list_CustomNodeCellEdit);
            // 
            // road_name
            // 
            this.road_name.Caption = "路线名称";
            this.road_name.FieldName = "路线名称";
            this.road_name.Name = "road_name";
            this.road_name.Visible = true;
            this.road_name.VisibleIndex = 0;
            this.road_name.Width = 160;
            // 
            // year
            // 
            this.year.Caption = "归档日期";
            this.year.FieldName = "归档日期";
            this.year.Name = "year";
            this.year.Visible = true;
            this.year.VisibleIndex = 1;
            this.year.Width = 40;
            // 
            // up_down_line
            // 
            this.up_down_line.Caption = "路幅";
            this.up_down_line.FieldName = "路幅";
            this.up_down_line.Name = "up_down_line";
            this.up_down_line.Visible = true;
            this.up_down_line.VisibleIndex = 2;
            this.up_down_line.Width = 40;
            // 
            // select
            // 
            this.select.Caption = "选择";
            this.select.FieldName = "选择";
            this.select.Name = "select";
            this.select.Visible = true;
            this.select.VisibleIndex = 3;
            this.select.Width = 20;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.ReadOnly = true;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            this.repositoryItemTextEdit2.ReadOnly = true;
            // 
            // button_indent2
            // 
            this.button_indent2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button_indent2.BackgroundImage = global::WindowsApplication1.Properties.Resources.pixelicious_2122;
            this.button_indent2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_indent2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_indent2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button_indent2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_indent2.Font = new System.Drawing.Font("Arial", 9F);
            this.button_indent2.ForeColor = System.Drawing.Color.Transparent;
            this.button_indent2.Location = new System.Drawing.Point(475, 215);
            this.button_indent2.Name = "button_indent2";
            this.button_indent2.Size = new System.Drawing.Size(42, 25);
            this.button_indent2.TabIndex = 21;
            this.button_indent2.TabStop = false;
            this.button_indent2.Text = " ";
            this.button_indent2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button_indent2.UseVisualStyleBackColor = true;
            this.button_indent2.Click += new System.EventHandler(this.button_indent2_onClick);
            this.button_indent2.MouseCaptureChanged += new System.EventHandler(this.button_indent2_MouseCaptureChanged);
            // 
            // google_earth
            // 
            this.google_earth.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.google_earth.BackgroundImage = global::WindowsApplication1.Properties.Resources.earth;
            this.google_earth.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.google_earth.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.google_earth.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.google_earth.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.google_earth.Font = new System.Drawing.Font("Arial", 9F);
            this.google_earth.ForeColor = System.Drawing.Color.Transparent;
            this.google_earth.Location = new System.Drawing.Point(560, 215);
            this.google_earth.Name = "google_earth";
            this.google_earth.Size = new System.Drawing.Size(42, 25);
            this.google_earth.TabIndex = 22;
            this.google_earth.TabStop = false;
            this.google_earth.Text = " ";
            this.google_earth.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.google_earth.UseVisualStyleBackColor = true;
            this.google_earth.Click += new System.EventHandler(this.google_earth_Click);
            // 
            // Form_gis_road
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(992, 454);
            this.ContextMenuStrip = this.cms;
            this.Controls.Add(this.google_earth);
            this.Controls.Add(this.button_indent2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.button_indent);
            this.Controls.Add(this.checkBoxLow);
            this.Controls.Add(this.checkBoxHigh);
            this.Controls.Add(this.comboBoxYear);
            this.Controls.Add(this.treeListGis);
            this.Controls.Add(this.road_select_list);
            this.Controls.Add(this.treeView_road_info);
            this.Controls.Add(this.webBrowser_google_earth);
            this.Controls.Add(this.axMap1);
            this.ForeColor = System.Drawing.SystemColors.Window;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximumSize = new System.Drawing.Size(10000, 10000);
            this.Name = "Form_gis_road";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_gis_road";
            this.SizeChanged += new System.EventHandler(this.Form_gis_road_SizeChanged);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form_gis_road_MouseClick);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form_gis_road_MouseUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_gis_road_MouseDown);
            this.Load += new System.EventHandler(this.Form_gis_road_Load);
            this.cms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axMap1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListGis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.road_select_list)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFile;
        //       private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        protected AxMapXLib.AxMap axMap1;
        ContextMenuStrip cms;
        private TreeView treeView_road_info;

        private Point img_gaoguan_logo_point = new Point(10, 10);
        private Image img_gaoguan_logo;
        private WebBrowser webBrowser_google_earth;// = ((System.Drawing.Image)(resources.GetObject("")));
        private string google_earth_navigate_site = "http://wuxianghui.100free.com/new.html";
        private int is_navigate = 0;
        private DevExpress.XtraTreeList.TreeList treeListGis;
        private DevExpress.XtraTreeList.Columns.TreeListColumn name;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tuli;
        private DevExpress.XtraTreeList.Columns.TreeListColumn check;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private ComboBox comboBoxYear;
        private CheckBox checkBoxHigh;
        private CheckBox checkBoxLow;
        private ToolStrip toolStrip1;
        private ToolStripButton toolStripButton7;
        private ToolStripButton toolStripButton2;
        private ToolStripButton toolStripButton3;
        private ToolStripButton toolStripButton14;
        private ToolStripButton toolStripButton11;
        private ToolStripButton toolStripButton10;
        private ToolStripButton toolStripButton8;
        private ToolStripButton toolStripButton6;
        private ToolStripButton toolStripGoogleEarth;
        private ToolStripPanel BottomToolStripPanel;
        private ToolStripPanel TopToolStripPanel;
        private ToolStripPanel RightToolStripPanel;
        private ToolStripPanel LeftToolStripPanel;
        private ToolStripContentPanel ContentPanel;
        private Button button_indent;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraTreeList.TreeList road_select_list;
        private DevExpress.XtraTreeList.Columns.TreeListColumn up_down_line;
        private DevExpress.XtraTreeList.Columns.TreeListColumn year;
        private DevExpress.XtraTreeList.Columns.TreeListColumn select;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn road_name;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private Button button_indent2;
        private ToolStripMenuItem googleEarthToolStripMenuItem;
        private ToolStripMenuItem 显示基本信息ToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private Button google_earth;
    }
}

