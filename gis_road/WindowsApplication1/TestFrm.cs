﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WindowsApplication1
{
    public partial class TestFrm : DevExpress.XtraEditors.XtraForm
    {
        public TestFrm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string road_name = this.textBox1.Text.Trim();
            GIS.DataSql dtSQL = new GIS.DataSql();
         DataTable dt=dtSQL.getGuiDangRiQi(road_name);
         this.dataGridView1.DataSource = dt.DefaultView;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string road_name = this.textBox1.Text.Trim();
            string table_name = this.textBox3.Text.Trim();
            string field_name = this.textBox2.Text.Trim();
            GIS.DataSql dtSQL = new GIS.DataSql();
            DataTable dt = dtSQL.getJiChuShuJu(road_name,table_name,field_name);
            this.dataGridView1.DataSource = dt.DefaultView;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string road_name = this.textBox1.Text.Trim();
            string table_name = this.textBox3.Text.Trim();
            string field_name = this.textBox2.Text.Trim();
           DateTime riqi =DateTime.Parse( this.textBox4.Text.Trim());
            string lufu = this.textBox5.Text.Trim();
            GIS.DataSql dtSQL = new GIS.DataSql();
            DataTable dt = dtSQL.getJianCeShuJu(road_name,table_name,field_name,riqi,lufu);
            this.dataGridView1.DataSource = dt.DefaultView;
        }
    }
}