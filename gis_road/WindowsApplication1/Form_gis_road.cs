using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Xml;
using System.Runtime.InteropServices;

using System.Reflection;
using System.Diagnostics;
using System.Net;
using mshtml;
using System.Timers;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using GIS;
namespace WindowsApplication1
{

    public partial class Form_gis_road : Form
    {

        ttttttttttt
        #region

        const string algorithm_dll_path = "c://gst_map3//dll_test1.dll";

        [DllImport(algorithm_dll_path)]
        public static extern void test_road1(ref UInt32 pp_gis_road_handle);

        [DllImport(algorithm_dll_path)]
        public static extern void test_road2(UInt32 pp_gis_road_handle);

        [DllImport(algorithm_dll_path)]
        public static extern void init_road_set(ref UInt32 pp_gis_road_handle);

        [DllImport(algorithm_dll_path)]
        public static extern void uninit_road_set(ref UInt32 pp_gis_road_handle);

        [DllImport(algorithm_dll_path)]
        public static extern void do_equalization(UInt32 gis_road_handle, double[] road_point, int road_point_number, double road_length);

        [DllImport(algorithm_dll_path)]
        public static extern void set_road_seg(UInt32 gis_road_handle, int[] road_seg_arr, uint[] road_seg_color_arr, int seg_number);

        [DllImport(algorithm_dll_path)]
        public static extern int get_road_seg_number(UInt32 gis_road_handle);

        [DllImport(algorithm_dll_path)]
        public static extern int get_road_seg_by_index(UInt32 gis_road_handle, int index, ref int from, ref int to, ref uint color);

        [DllImport(algorithm_dll_path)]
        public static extern int get_road_seg_point_by_index(UInt32 gis_road_handle, int index, ref double x, ref double y, ref uint color);





        #endregion
        #region
        //  private AxMapXLib.AxMap axMap1;  form.designer 里有这段话
        //  private AxMapXLib.AxMap axMap2;

        private double zoom;
        private double centerx;
        private double centery;


        #endregion



        #region
        public class color_panel
        {
            public Color color;
            public string icon_color_path;
        };
        public class gis_road_basic_information_draw_unit
        {

        };

        public class gis_road_detection_information_unit
        {
            public string lable;// type name
            public int node_leaf; //0 -- node; 1-- leaf;
            public double value1;
            public double value2;
            public Color color;
            public string other_info;
            public ArrayList child;
            
            public gis_road_detection_information_unit()
            {
                color = new Color();
                child = null;
            } // node or leaf
        }; // node or leaf

        public class gis_road_detection_type
        {
            public string lable;
            public string table_name;
            public string field_name;
            public double[] road_seg_value;
        };
        public class gis_road_detection_unit
        {
            public int road_length;
            public UInt32 ref_road_draw_unit;
            public ArrayList gis_road_detection_type_arr;
            public uint[] road_seg_color; //used to draw the seg;
            //  public double[] road
        };
        public class gis_road_basic_information_unit_types
        {
            public string label;
            public uint color;
        };
        public class gis_road_basic_information_unit
        {
            public string road_name;//路面名称
            public int road_feature_id;
            public ArrayList archive_date;
            public Dictionary<string, road_type_color_value> basic_information_unit_types;

            public uint road_opening; //通车时间
            public uint road_degree;  //路面等级
            public uint road_channels; //车道数
            public uint road_type; //路面类型
            public uint road_suface_struct; //路面表层结构
            public PointF[] points;
            public gis_road_detection_unit road_detection_unit;
            public gis_road_basic_information_unit()
            {
                basic_information_unit_types = new Dictionary<string, road_type_color_value>();
            }
            public double[] latitude_longitude;
            public string tip_link_url_pic;
            public string tip_info_string;
            public string tip_link_url;
            public bool is_draw;
        };
        public class color_tree_node
        {
            public Color color;
            public tree_node_value_interval tnvi;
            public color_tree_node()
            {
                color = new Color();
                tnvi = new tree_node_value_interval();
            }
        };
        private gis_road_basic_information_unit[] gis_road_basic_information_unit_index = null;
        color_panel[] color_panel_index = null;
        private string layer_name;
        private int draw_type = 0;// 
        private string draw_type_string = "路面类型";// DrawGisMapssTest
        private int draw_type_color_index = -1;// default
        private string _LU_XIAN_MING_CHENG_ = "路线名称（google earth）";
        private string _XUAN_ZE_LU_XIAN_ = "选择路线";
        #endregion

        int cur_times = 0;
        public class tree_node_value_interval 
        {    
            public double value_low;
            public double value_high;
        };
        void init_road_detection_unit(string label,string table_name,string  field_name)
        {
            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
                if (gis_road_basic_information_unit_index[i].road_detection_unit == null)
                    gis_road_basic_information_unit_index[i].road_detection_unit = new gis_road_detection_unit();
                if (gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr == null)
                    gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr = new ArrayList();
                gis_road_detection_type temp = new gis_road_detection_type();
                temp.lable = label;
                temp.table_name = table_name;
                temp.field_name = field_name;
                temp.road_seg_value = null;
                gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr.Add(temp);
            }
        }
        void get_archive_date()
        {
        }
        void init_road_basic_info_uinit(string label, string table_name, string field_name)
        {
            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
                if (gis_road_basic_information_unit_index[i].basic_information_unit_types == null)
                    gis_road_basic_information_unit_index[i].basic_information_unit_types = new Dictionary<string, road_type_color_value>();
                if (!gis_road_basic_information_unit_index[i].basic_information_unit_types.ContainsKey(label))
                {
                    road_type_color_value temp = new road_type_color_value();
                    temp.color = 0xff0ff10f;
                    temp.value = 0;
                    temp.table_name = table_name;
                    temp.field_name = field_name;
                    gis_road_basic_information_unit_index[i].basic_information_unit_types.Add(label, temp);
                }
            }
        }
        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                //清空的代码
            }
        }

        private void init_google_earth()
        {
            if (is_navigate == 0)
            {
                //    this.webBrowser_google_earth.Navigate(google_earth_navigate_site);
                this.webBrowser_google_earth.Url = new Uri(google_earth_navigate_site);
                
                this.webBrowser_google_earth.Visible = false;
                is_navigate = 1;
            }

        }

        private void change_to_google_earth()
        {
            if (this.webBrowser_google_earth.Visible == true)
                return;
            this.axMap1.Visible = false;
            this.webBrowser_google_earth.Visible = true;
            if (is_navigate == 0)
            {
                //    this.webBrowser_google_earth.Navigate(google_earth_navigate_site);
                this.webBrowser_google_earth.Url = new Uri(google_earth_navigate_site);
                is_navigate = 1;

            }
            toolStrip1.Visible = false;

        }
        private void change_to_map()
        {

            if (this.axMap1.Visible == true)
                return;
            this.axMap1.Visible = true;
            this.webBrowser_google_earth.Visible = false;
            toolStrip1.Visible = true;
        }
        private void do_basic_info_action(TreeNode node_t)
        {
            //  change_to_map();
            if (node_t.Parent != null)
            {
                if (node_t.Parent.Text.CompareTo("基础数据") == 0)
                {
                    int ccc = node_t.Nodes.Count;
                    double[] v_l = new double[ccc];
                    double[] v_h = new double[ccc];
                    uint[] colors = new uint[ccc];
                    bool[] is_Checked = new bool[ccc];
                    string label = node_t.Text;
                    int k = 0;
                    foreach (TreeNode t in node_t.Nodes)
                    {
                        tree_node_value_interval te = (tree_node_value_interval)t.Tag;
                        v_l[k] = te.value_low;
                        v_h[k] = te.value_high;
                        colors[k] = (uint)((t.BackColor.A << 24) | (t.BackColor.R << 16) | (t.BackColor.G << 8) | (t.BackColor.B));
                        if (t.Checked)
                            is_Checked[k] = true;
                        else
                            is_Checked[k] = false;

                        k++;
                    }
                    {
                        for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                        {

                            road_type_color_value col = null;
                            gis_road_basic_information_unit_index[i].basic_information_unit_types.TryGetValue(label, out col);
                            for (int ab = 0; ab < v_l.Length; ab++)
                            {
                                if ((col.value >= v_l[ab]) && (col.value < v_h[ab]))
                                {
                                    if (is_Checked[ab] == true)
                                        col.color = colors[ab];
                                    else
                                        col.color = 0x00000000;

                                    break;

                                }
                            }

                        }
                    }
                    draw_type_string = node_t.Text;
                    draw_type = 0;

                }

            }
        }
        private void refresh_temp_layer()
        {
            if (gis_road_basic_information_unit_index != null)
            {
                road_gis_find_features(layer_name);
                remove_all_temp_feature();
            DrawGisMapssTest( draw_type_string);
            }
        }
        private void do_basic_info_action1(TreeListNode node_t)
        {

            if (node_t.ParentNode != null)
            {
                if (node_t.ParentNode.GetValue(0).ToString().CompareTo("基础数据") == 0)
                {
                    int ccc = node_t.Nodes.Count;
                    double[] v_l = new double[ccc];
                    double[] v_h = new double[ccc];
                    uint[] colors = new uint[ccc];
                    bool[] is_Checked = new bool[ccc];
                    string label = node_t.GetValue(0).ToString();
                    int k = 0;
                    foreach (TreeListNode t in node_t.Nodes)
                    {
                        tree_node_value_interval te = (tree_node_value_interval)t.Tag;
                        String BackColor = t.GetValue(1).ToString();
                        v_l[k] = te.value_low;
                        v_h[k] = te.value_high;
                        colors[k] = (uint)Convert.ToUInt32(BackColor);
                        //if (t.Checked)
                        is_Checked[k] = (bool)t.GetValue(2);
                        is_Checked[k] = true;
                        k++;
                    }
                    {
                        for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                        {

                            road_type_color_value col = null;
                            gis_road_basic_information_unit_index[i].basic_information_unit_types.TryGetValue(label, out col);
                            for (int ab = 0; ab < v_l.Length; ab++)
                            {
                                if ((col.value >= v_l[ab]) && (col.value < v_h[ab]))
                                {
                                    if (is_Checked[ab] == true)
                                    {
                                        col.color = colors[ab];
                                        gis_road_basic_information_unit_index[i].is_draw = true;
                                    }
                                    else
                                        gis_road_basic_information_unit_index[i].is_draw = false;

                                    break;

                                }
                            }

                        }
                    }
                    draw_type_string = node_t.GetValue(0).ToString();
                    draw_type = 0;

            //        axMap1.Refresh();
                    refresh_temp_layer();

                }

            }
        }
        private DataTable getShuJu(string roadName, DateTime RiQi, string LuFu, string LeiXing, string field)
        {
            return null;
        }
        private void do_tree_click_action1(object sender, MouseEventArgs e)
        {

            {
                if (earth_action_span_time == 1)
                    return;

            }

            if (treeListGis.FocusedNode == null)
                return;
                             TreeListNode node_t = treeListGis.FocusedNode;
                    TreeListNode p_node_t = node_t.ParentNode;
                    TreeListNode c_node_t =null;
            if (e.Button == MouseButtons.Left)
            {
                {
   
                    if(node_t.HasChildren)
                     c_node_t = node_t.Nodes[0];
                    p_node_t = node_t.ParentNode;

                    if (c_node_t != null)
                    {
                        if (c_node_t.HasChildren)
                            return;
                    }
                    if(p_node_t!=null)
                    {
                        while (p_node_t.ParentNode != null)
                        {
                            p_node_t = p_node_t.ParentNode;
                        }
                    }
                    if (node_t.ParentNode != null)
                    {
                        String aaa = node_t.ParentNode.GetValue(0).ToString();
                        if (p_node_t.GetValue(0).ToString().CompareTo("基础数据") == 0)
                        {

                        //    if (treeListGis.FocusedNode.Nodes.Count == 0)
                        //    {
                        //        treeListGis.FocusedNode = treeListGis.FocusedNode.ParentNode;
                        //    }
                            change_to_map();
                            do_basic_info_action1(treeListGis.FocusedNode);

                        }
                        else if (p_node_t.GetValue(0).ToString().CompareTo("检测数据") == 0)
                        {
                          //  if (treeListGis.FocusedNode.Nodes.Count == 0)
                         //   {
                          //      treeListGis.FocusedNode = treeListGis.FocusedNode.ParentNode;
                           // }
                            change_to_map();
                            draw_type = 5;

                        }
                        else if (node_t.ParentNode.GetValue(0).ToString().CompareTo(_LU_XIAN_MING_CHENG_) == 0)
                        {
                            
                            {
                              //  settime(2000);
                                change_to_google_earth();

                                for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                                {
                                    if (gis_road_basic_information_unit_index[i].road_name.CompareTo(treeListGis.FocusedNode.GetValue(0).ToString()) == 0)
                                    {
                                        do_google_earth_act(treeListGis.FocusedNode.GetValue(0).ToString(), gis_road_basic_information_unit_index[i].latitude_longitude[0],
                                            gis_road_basic_information_unit_index[i].latitude_longitude[1], gis_road_basic_information_unit_index[i].tip_link_url_pic,
                                            treeListGis.FocusedNode.GetValue(0).ToString(), gis_road_basic_information_unit_index[i].tip_link_url);
                                    }
                                }
                            }
                            return;
                        }
                    }
                }

                if (draw_type == 5)
                {
                    tree_node_value_interval pt = (tree_node_value_interval)treeListGis.FocusedNode.Tag;
                 //   c_node_t=(tree_node_value_interval)treeListGis.FocusedNode.Nodes[]
                    if (treeListGis.FocusedNode.Nodes.Count == 0)
                        return;
                    color_tree_node[] ctn=new color_tree_node[treeListGis.FocusedNode.Nodes.Count];
                    for (int j = 0; j < ctn.Length; j++)
                    {
                        ctn[j] = new color_tree_node();
                    }
                    int ki=0;
                    foreach (TreeListNode tempN in treeListGis.FocusedNode.Nodes)
                    {
                  //      ctn[ki].  //    (tree_node_value_interval)tempN.Tag;


                        String cc = (String)tempN.GetValue(1);
                        Color BackColor=Color.Blue;
                        if (cc != null)
                        {
                            uint icolor = Convert.ToUInt32(cc, 10);
                            BackColor = Color.FromArgb(255, (int)(icolor & 0x00ff0000) >> 16,
                                     (int)(icolor & 0x0000ff00) >> 8, (int)(icolor & 0x000000ff));

                        }

                        ctn[ki].tnvi = (tree_node_value_interval)tempN.Tag;
                        ctn[ki].color = BackColor;

                        ki++;
                    }
                 //                          if (pt == null)
                 //       return;
                 //   color_tree_node ctn=new color_tree_node[];
                     //   public class tree_node_value_interval color_tree_node
                    string label = treeListGis.FocusedNode.GetValue(0).ToString();
                 //  string aa = Convert.ToString(pt.value_low);
                 //  string bb = Convert.ToString(pt.value_high);
                 //   MessageBox.Show( aa+"  " +bb,"", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                    uint r = 255;
                    uint g = 0;
                    uint b = 255;
                    int aaa = 10;
                    String suffix = "-1-1";
                    for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                    {
                        if (gis_road_basic_information_unit_index[i].road_detection_unit != null)
                        {
                            if (gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr != null)
                            {
                                foreach (gis_road_detection_type temp in gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr)
                                {
                                    if (label.CompareTo(temp.lable) == 0)
                                    {
                                        // do the color seg
                                        
                                        gis_road_basic_information_unit_index[i].road_detection_unit.road_seg_color = new uint[gis_road_basic_information_unit_index[i].road_detection_unit.road_length];
                                       
                                        DataTable tb=null;
                                        String lufus;
                                        if(checkBoxHigh.Checked)//
                                            lufus="上行";
                                        else
                                            lufus="下行";
                                        p_node_t = node_t.ParentNode;

                                 /*       tb = getShuJu(gis_road_basic_information_unit_index[i].road_name, DateTime.Parse(comboBoxYear.Text + suffix), lufus,
                                           node_t.ParentNode.GetValue(0).ToString(), node_t.GetValue(0).ToString());
                                       if (tb == null)
                                           return;
                                        */

                                        string t_roadname=null;
                                        DateTime t_at=new DateTime(2001, 12, 14);;
                                        bool t_c = false; ;
                                        string t_shang_xia=null;
                                        foreach (TreeListNode tnode in road_select_list.Nodes)
                                        {
                                     
                                            t_shang_xia = null;
                                            t_c = false;
                                            t_roadname = (string)tnode.GetValue(0);
                                            if (t_roadname == null)
                                                continue;
                                            if (t_roadname.CompareTo(gis_road_basic_information_unit_index[i].road_name) == 0)
                                            {
                                                t_shang_xia = (string)tnode.GetValue(2);
                                                t_c = (bool)tnode.GetValue(3);
                                                t_at = (DateTime)tnode.GetValue(1);
                                                break;
                                            }
                                        }
                                        if (t_c == false)
                                        {
                                            gis_road_basic_information_unit_index[i].is_draw = false;
                                            continue;
                                        }
                                        if (t_roadname == null)
                                        {
                                            gis_road_basic_information_unit_index[i].is_draw = false;
                                            continue;
                                        }
                                        gis_road_basic_information_unit_index[i].is_draw = t_c;
                                        DataSql ds = new DataSql();





                                        if (t_at == null)
                                            continue;
                                        tb = ds.getJianCeShuJu(gis_road_basic_information_unit_index[i].road_name,
                                            temp.table_name, temp.field_name, t_at, t_shang_xia);
                                        if (tb == null)
                                        {
                                            gis_road_basic_information_unit_index[i].is_draw = false;
                                            continue;
                                        }
                                         

                                        init_road_detection_db_data_by_tree_node( gis_road_basic_information_unit_index[i],
                                              temp,node_t.ParentNode.GetValue(0).ToString(), tb);  // DataRow[] drss = dt.Select("column0 = 'AX'");
                                        
                                        gis_road_basic_information_unit_index[i].road_detection_unit.road_seg_color = new uint[gis_road_basic_information_unit_index[i].road_detection_unit.road_length];
                                        for (int ji = 0; ji < temp.road_seg_value.Length; ji++)
                                        {
                                            gis_road_basic_information_unit_index[i].road_detection_unit.road_seg_color[ji]=(uint)(0xff0ef0ff);
                                            for (int jj = 0; jj < ctn.Length; jj++)
                                            {
                                                if ((ctn[jj].tnvi.value_low <= temp.road_seg_value[ji]) && (ctn[jj].tnvi.value_high > temp.road_seg_value[ji]))
                                                {
                                                    gis_road_basic_information_unit_index[i].road_detection_unit.road_seg_color[ji] = (uint)
                                                        (uint)(ctn[jj].color.A << 24) | ((uint)(ctn[jj].color.R) << 16) | ((uint)ctn[jj].color.G << 8 | (uint)ctn[jj].color.B);
                                                    break;
                                                }
                                            }
                                        }
                                    
                                        //do_basic_info_action1(treeListGis.FocusedNode);
                                        break;
                                    }
                                }
                            }
                            //  temp.road_seg_value = null;
                            //  gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr.Add(temp);
                        }

                    }
               //     axMap1.Refresh();
                }
                change_to_map();
                refresh_temp_layer();
                //清空的代码
            }
        }

        private void do_tree_click_action(object sender, MouseEventArgs e)
        {

            {
                if (earth_action_span_time == 1)
                    return;

            }

            if (treeView_road_info.SelectedNode == null)
                return;

            if (e.Button == MouseButtons.Left)
            {
                {
                    TreeNode node_t = treeView_road_info.SelectedNode;
                    if (node_t.Parent != null)
                    {
                        if (node_t.Parent.Text.CompareTo("基础数据") == 0)
                        {
                            if (treeView_road_info.SelectedNode.Nodes.Count == 0)
                            {
                                treeView_road_info.SelectedNode = treeView_road_info.SelectedNode.Parent;
                            }
                            change_to_map();
                            do_basic_info_action(treeView_road_info.SelectedNode);

                        }
                        else if (node_t.Text.CompareTo("检测数据") == 0)
                        {
                            if (treeView_road_info.SelectedNode.Nodes.Count == 0)
                            {
                                treeView_road_info.SelectedNode = treeView_road_info.SelectedNode.Parent;
                            }
                            change_to_map();
                            draw_type = 5;

                        }
                        else if (node_t.Parent.Text.CompareTo(_LU_XIAN_MING_CHENG_) == 0)
                        {
                           
                            if (is_navigate == 0)
                            {
                                settime(8000);
                                change_to_google_earth();
                            }
                            else
                            {
                                settime(2000);
                                change_to_google_earth();

                                for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                                {
                                    if (gis_road_basic_information_unit_index[i].road_name.CompareTo(treeView_road_info.SelectedNode.Text) == 0)
                                    {
                                        do_google_earth_act(treeView_road_info.SelectedNode.Text, gis_road_basic_information_unit_index[i].latitude_longitude[0],
                                            gis_road_basic_information_unit_index[i].latitude_longitude[1], gis_road_basic_information_unit_index[i].tip_link_url_pic,
                                            treeView_road_info.SelectedNode.Text, gis_road_basic_information_unit_index[i].tip_link_url);
                                    }
                                }
                            }
                            return;
                        }
                    }
                }

                if (draw_type == 5)
                {
                    tree_node_value_interval pt = (tree_node_value_interval)treeView_road_info.SelectedNode.Tag;
                    if (pt == null)
                        return;


                    string label = treeView_road_info.SelectedNode.Parent.Text;

                    string aa = Convert.ToString(pt.value_low);
                    string bb = Convert.ToString(pt.value_high);

                    //   MessageBox.Show( aa+"  " +bb,"", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                    uint r = 255;
                    uint g = 0;
                    uint b = 255;
                    int aaa = 10;
                    for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                    {
                        if (gis_road_basic_information_unit_index[i].road_detection_unit != null)
                        {
                            if (gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr != null)
                            {
                                foreach (gis_road_detection_type temp in gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr)
                                {
                                    if (label.CompareTo(temp.lable) == 0)
                                    {
                                        // do the color seg
                                        gis_road_basic_information_unit_index[i].road_detection_unit.road_seg_color = new uint[gis_road_basic_information_unit_index[i].road_detection_unit.road_length];
                                        for (int ku = 0; ku < gis_road_basic_information_unit_index[i].road_detection_unit.road_length; ku++)
                                        {

                                            Random rnd1 = new Random(System.DateTime.Now.Millisecond);
                                            Random rnd2 = new Random(ku + 10);
                                            Random rnd3 = new Random(ku + 12);

                                            aaa++;
                                            if (aaa % 10 == 0)
                                            {
                                                r = (uint)rnd1.Next(10, 255);
                                                g = (uint)rnd2.Next(10, 255);
                                                b = (uint)rnd3.Next(10, 255);
                                            }

                                            gis_road_basic_information_unit_index[i].road_detection_unit.road_seg_color[ku] = (uint)(0xff000000 | r << 24 | g << 16 | b);
                                        }
                                        break;
                                    }
                                }
                            }
                            //  temp.road_seg_value = null;
                            //  gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr.Add(temp);
                        }

                    }
                }

                //清空的代码
            }
        }
        private void treeView_road_info_NodeMouseDoubleClick(object sender, MouseEventArgs e)
        {
            //  treeView_road_info.ExpandAll();
            do_tree_click_action(sender, e);
        }
        void get_child_node1(XmlElement parent, TreeListNode nodes)
        {
            if (parent.ChildNodes.Count > 0)
                foreach (XmlElement birthday in parent.ChildNodes)
                {
                    //读取数据 
                    if (birthday != null)
                    {
                        string type = birthday.Name;
                        string label = birthday.GetAttribute("label");
                        string plabel = parent.GetAttribute("label");
                        if (parent.GetAttribute("label") != null)
                        {
                            if (plabel.CompareTo("基础数据") == 0)
                            {
                                string table_name = birthday.GetAttribute("table_name");
                                string field_name = birthday.GetAttribute("field_name");
                                init_road_basic_info_uinit(label, table_name,field_name);
                            }

                        }
                        TreeListNode node = null;
                        if (birthday.HasChildNodes)
                        {
                            if (!birthday.FirstChild.HasChildNodes)
                            {
                                string table_name = birthday.GetAttribute("table_name");
                                string field_name = birthday.GetAttribute("field_name");
                                init_road_detection_unit(label, table_name, field_name);
                         //       if(nodes.LastNode!=null)
                         //       append_lu_duan_ming_cheng(nodes.LastNode);
                            }

                            node = treeListGis.AppendNode(new object[] { label, null }, nodes);
                        }
                        else
                        {

                            string s_v_l = birthday.GetAttribute("value_low");
                            string s_v_h = birthday.GetAttribute("value_high");
                            double v_l = 0;
                            double v_h = 0;
                            if (s_v_l != null)
                                v_l = Convert.ToDouble(s_v_l);
                            if (s_v_h != null)
                                v_h = Convert.ToDouble(s_v_h);

                            tree_node_value_interval te = new tree_node_value_interval();
                            te.value_low = v_l;
                            te.value_high = v_h;


                            string colors = birthday.GetAttribute("color");
                            uint icolor = Convert.ToUInt32(colors, 16);

                            Color BackColor = Color.FromArgb(255, (int)(icolor & 0x00ff0000) >> 16,
                                   (int)(icolor & 0x0000ff00) >> 8, (int)(icolor & 0x000000ff));
                            Color ForeColor = Color.FromArgb(255, 255 - ((int)(icolor & 0x00ff0000) >> 16),
                                   255 - ((int)(icolor & 0x0000ff00) >> 8), 255 - ((int)(icolor & 0x000000ff)));

                            // Bitmap bitmap = new Bitmap("c:\\vmedian.bmp");

                            Bitmap objBitmap = new Bitmap(160, 60);
                            Graphics objGraphics = Graphics.FromImage(objBitmap);
                            objGraphics.FillRectangle(new SolidBrush(BackColor), 0, 0, 160, 60);
                            objGraphics.Dispose();

                            //  Image myImage = Image.FromFile("c:\\vmedian.bmp");
                            String co = icolor.ToString();
                            node = treeListGis.AppendNode(new object[] { label, co, false }, nodes);

                            node.Tag = te;


                            //Color fe = node.BackColor;

                        }
                        get_child_node1(birthday, node);
                        cur_times++;
                    }
                }

        }
        void get_child_node(XmlElement parent, TreeNode nodes)
        {
            if (parent.ChildNodes.Count > 0)
                foreach (XmlElement birthday in parent.ChildNodes)
                {
                    //读取数据 
                    if (birthday != null)
                    {
                        string type = birthday.Name;
                        string label = birthday.GetAttribute("label");
                        string plabel = parent.GetAttribute("label");
                        if (parent.GetAttribute("label") != null)
                        {
                            if (plabel.CompareTo("基础数据") == 0)
                            {
                             //   init_road_basic_info_uinit(label);
                                treeView_road_info.SelectedNode = nodes;
                            }

                        }
                        TreeNode node = nodes.Nodes.Add(label);
                        get_child_node(birthday, node);
                        if (birthday.HasChildNodes)
                        {
                            if (!birthday.FirstChild.HasChildNodes)
                            {
                             //   init_road_detection_unit(label);
                            }
                        }
                        else
                        {

                            string s_v_l = birthday.GetAttribute("value_low");
                            string s_v_h = birthday.GetAttribute("value_high");
                            double v_l = 0;
                            double v_h = 0;
                            if (s_v_l != null)
                                v_l = Convert.ToDouble(s_v_l);
                            if (s_v_h != null)
                                v_h = Convert.ToDouble(s_v_h);

                            tree_node_value_interval te = new tree_node_value_interval();
                            te.value_low = v_l;
                            te.value_high = v_h;

                            node.Tag = te;

                            string colors = birthday.GetAttribute("color");
                            uint icolor = Convert.ToUInt32(colors, 16);


                            node.BackColor = Color.FromArgb(255, (int)(icolor & 0x00ff0000) >> 16,
                                (int)(icolor & 0x0000ff00) >> 8, (int)(icolor & 0x000000ff));
                            node.ForeColor = Color.FromArgb(255, 255 - ((int)(icolor & 0x00ff0000) >> 16),
                                255 - ((int)(icolor & 0x0000ff00) >> 8), 255 - ((int)(icolor & 0x000000ff)));
                            Color fe = node.BackColor;

                        }
                        cur_times++;
                    }
                }

        }
        void init_road_lu_duan_ming_cheng()
        {
            return;
            string guan_xia_lu_duan = _LU_XIAN_MING_CHENG_;
            TreeNode node = treeView_road_info.Nodes.Add(guan_xia_lu_duan);

            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
                node.Nodes.Add(gis_road_basic_information_unit_index[i].road_name);
            }


        }
        /*
        private void InitEditors()
        {
            DevExpress.XtraEditors.Repository.RepositoryItemComboBox cb = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            treeList1.RepositoryItems.Add(cb);
            DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit se = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            treeList1.RepositoryItems.Add(se);

            string s, oldLocation = "";
            DataView dv = new DataView(dataView.Table);
            dv.Sort = "[Location] ASC";
            for (int i = 0; i < dv.Count; i++)
            {
                s = dv[i].Row.ItemArray.GetValue(5) as string;
                if (s != oldLocation)
                    cb.Items.Add(s);
                oldLocation = s;
            }
            cb.DropDownRows = 10;

            DevExpress.XtraTreeList.Columns.TreeListColumn col;

            treeList1.Columns["Location"].ColumnEdit = cb;
            col = treeList1.Columns["Department"];
            col.SummaryFooter = SummaryItemType.Count;
            col.AllNodesSummary = true;
            col = treeList1.Columns["Budget"];
            col.ColumnEdit = se;
            col.Format.FormatString = "c";
            col.RowFooterSummary = col.SummaryFooter = SummaryItemType.Sum;
            col.RowFooterSummaryStrFormat = col.SummaryFooterStrFormat = "Sum={0:c}";
            col.AllNodesSummary = true;
            treeList1.BestFitColumns();
            col.Width = 110;
        }
        */
        void init_road_lu_duan_ming_cheng1()
        {
              // init_road_select_list_data();
            
            string guan_xia_lu_duan = _LU_XIAN_MING_CHENG_;
            
            /*
            String[] aa={"2001","2002"};
            TreeListNode node = road_select_list.AppendNode(new object[] { guan_xia_lu_duan, null }, null);
            
            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
                String pp="200"+i;
                road_select_list.AppendNode(new object[] { gis_road_basic_information_unit_index[i].road_name, pp, "上行", false }, node);
                gis_road_basic_information_unit_index[i].is_draw = true;
            }
             */
           
           // TreeListNode node = road_select_list.AppendNode(new object[] { guan_xia_lu_duan, null }, null);

            Record[] records = new Record[gis_road_basic_information_unit_index.Length];
         
            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
               
                records[i] = new Record(gis_road_basic_information_unit_index[i].road_name, new DateTime(2001, 12, 14), "上行", false, i);

                gis_road_basic_information_unit_index[i].is_draw = true;
            }


          

        //    DataTable data_table = new DataTable();
/* 
            if (e.Column.FieldName == "路线名称")
            {
                e.RepositoryItem = repositoryItemTextEdit2;
            }
            else if (e.Column.FieldName == "归档日期")
            {
                e.RepositoryItem = repositoryItemComboBox2;
            }
            else if (e.Column.FieldName == "路幅")
            {
                e.RepositoryItem = repositoryItemComboBox2;
            }
            else if (e.Column.FieldName == "选择")

               data_table.Columns.Add("路线名称", typeof(string));
            data_table.Columns.Add("归档日期", typeof(string));
            data_table.Columns.Add("路幅", typeof(string));
            data_table.Columns.Add("选择", typeof(string));
                treeList1.DataSource = dataView = dataSet.Tables[0];
                */
         //   road_select_list.DataSource = records;
//
 //           Record[] records = new Record[11];
       
          road_select_list.DataSource = records;
            road_select_list.ExpandAll();
        }


        void append_lu_duan_ming_cheng(TreeListNode pnode)
        {
            TreeListNode node = treeListGis.AppendNode(new object[] { _XUAN_ZE_LU_XIAN_, null }, pnode);
            string[] ar ={ "1981", "1832" };
            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
                int[] arg ={ 32, 324, 32 };
                TreeListNode temp = treeListGis.AppendNode(new object[] { gis_road_basic_information_unit_index[i].road_name, "1990", false }, node);
               // temp.
       //      bt.Items.Add("fewfw");
                gis_road_basic_information_unit_index[i].is_draw = false;
                //node.N
            }
        }


        void set_road_latitude_longitude(string name, double[] cor, string link_url_pic, string info, string link_url)
        {
            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
                if (gis_road_basic_information_unit_index[i].road_name.CompareTo(name) == 0)
                {
                    gis_road_basic_information_unit_index[i].latitude_longitude = new double[cor.Length];
                    for (int j = 0; j < cor.Length; j++)
                    {
                        gis_road_basic_information_unit_index[i].latitude_longitude[j] = cor[j];
                        gis_road_basic_information_unit_index[i].tip_link_url_pic = link_url_pic;
                        gis_road_basic_information_unit_index[i].tip_info_string = info;
                        gis_road_basic_information_unit_index[i].tip_link_url = link_url;
                    }
                }
            }

        }
        void init_road_la_lo()
        {
            XmlDocument dom = new XmlDocument();

            dom.Load("c://gst_map3//road_gis_position.xml");//装载XML文档 
            //遍历所有节点 

            foreach (XmlElement birthday in dom.DocumentElement.ChildNodes)
            {
                string tlabel;
                double la;
                double lo;
                if (birthday != null)
                {
                    string type = birthday.Name;
                    string label = birthday.GetAttribute("label");
                    if (label.CompareTo("路段经纬度") == 0)
                    {
                        if (birthday.HasChildNodes)
                        {
                            foreach (XmlElement tnode in birthday.ChildNodes)
                            {
                                tlabel = tnode.GetAttribute("label");
                                la = Convert.ToDouble(tnode.GetAttribute("v_la"));
                                lo = Convert.ToDouble(tnode.GetAttribute("v_lo"));
                                string link_url_pic = tnode.GetAttribute("link_url_picture");
                                string link_url = tnode.GetAttribute("link_url");
                                string info = tnode.GetAttribute("info");
                                double[] llao = new double[2];
                                llao[0] = la;
                                llao[1] = lo;
                                set_road_latitude_longitude(tlabel, llao, link_url_pic, info, link_url);

                            }

                        }
                        break;
                    }

                }
            }
        }
        /*
     
///////////////////   xml 特殊字符
        
&gt; > 大于号
&amp; & 和
&apos; ‘ 单引号
&quot; ” 双引号
  
         */
        // add the newest code
        // 
  
        private void init_road_detaction_structure1()
        {
            init_road_lu_duan_ming_cheng();
            init_road_la_lo();
            XmlDocument dom = new XmlDocument();

            dom.Load("c://gst_map3//road_gis.xml");//装载XML文档 
            //遍历所有节点 

            foreach (XmlElement birthday in dom.DocumentElement.ChildNodes)
            {
                if (birthday != null)
                {
                    string type = birthday.Name;
                    string label = birthday.GetAttribute("label");

                    TreeListNode node = null;
                    node = treeListGis.AppendNode(new object[] { label }, null);

                    get_child_node1(birthday, node);
                    if (birthday.HasChildNodes)
                    {
                        if (!birthday.FirstChild.HasChildNodes)
                        {
                            string table_name = birthday.GetAttribute("table_name");
                            string field_name = birthday.GetAttribute("field_name");
                            init_road_detection_unit(label, table_name, field_name);
                        }
                    }
                    cur_times++;
                }
            }
            this.treeListGis.ExpandAll();


            //   if (MessageBox.Show(s_test, "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.OK)
            //    {
            //
            //   }
        }
        ImageList myImageList = new ImageList();


        private void init_road_detaction_structure()
        {
            // init_road_lu_duan_ming_cheng();
            
            init_road_lu_duan_ming_cheng1();
            init_road_la_lo();
            return;
            XmlDocument dom = new XmlDocument();

            dom.Load("c://gst_map3//road_gis.xml");//装载XML文档 
            //遍历所有节点 

            foreach (XmlElement birthday in dom.DocumentElement.ChildNodes)
            {
                if (birthday != null)
                {
                    string type = birthday.Name;
                    string label = birthday.GetAttribute("label");
                    TreeNode node = treeView_road_info.Nodes.Add(label);


                    get_child_node(birthday, node);
                    if (birthday.HasChildNodes)
                    {
                        if (!birthday.FirstChild.HasChildNodes)
                        {
                          //  init_road_detection_unit(label);
                        }
                    }
                    cur_times++;
                }
            }
            treeView_road_info.ExpandAll();


            //   if (MessageBox.Show(s_test, "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.OK)
            //   {
            //
            //   }
        }

        private void init_color_panel()
        {
            color_panel_index = new color_panel[18];
            for (int i = 0; i < 18; i++)
            {
                color_panel_index[i] = new color_panel();
                color_panel_index[i].color = new Color();
            }

            color_panel_index[0].color = Color.FromArgb(244, 255, 41, 22);
            color_panel_index[0].icon_color_path = " ";

            color_panel_index[1].color = Color.FromArgb(244, 122, 255, 0);
            color_panel_index[1].icon_color_path = " ";

            color_panel_index[2].color = Color.FromArgb(244, 0, 105, 255);
            color_panel_index[2].icon_color_path = " ";

            color_panel_index[3].color = Color.FromArgb(244, 255, 54, 32);
            color_panel_index[3].icon_color_path = " ";

            color_panel_index[4].color = Color.FromArgb(244, 32, 0, 32);
            color_panel_index[4].icon_color_path = " ";

            color_panel_index[5].color = Color.FromArgb(244, 145, 0, 76);
            color_panel_index[5].icon_color_path = " ";

            color_panel_index[6].color = Color.FromArgb(244, 43, 122, 122);
            color_panel_index[6].icon_color_path = " ";

            color_panel_index[6].color = Color.FromArgb(244, 255, 34, 0);
            color_panel_index[6].icon_color_path = " ";

            color_panel_index[7].color = Color.FromArgb(244, 122, 20, 30);
            color_panel_index[7].icon_color_path = " ";

            color_panel_index[8].color = Color.FromArgb(244, 12, 0, 120);
            color_panel_index[8].icon_color_path = " ";

            color_panel_index[9].color = Color.FromArgb(244, 12, 30, 255);
            color_panel_index[9].icon_color_path = " ";

            color_panel_index[10].color = Color.FromArgb(244, 34, 255, 144);
            color_panel_index[10].icon_color_path = " ";

            color_panel_index[11].color = Color.FromArgb(244, 255, 0, 0);
            color_panel_index[11].icon_color_path = " ";

            color_panel_index[12].color = Color.FromArgb(244, 34, 43, 76);
            color_panel_index[12].icon_color_path = " ";

            color_panel_index[13].color = Color.FromArgb(244, 255, 0, 0);
            color_panel_index[13].icon_color_path = " ";

            color_panel_index[14].color = Color.FromArgb(244, 3, 44, 44);
            color_panel_index[14].icon_color_path = " ";

            color_panel_index[15].color = Color.FromArgb(244, 32, 232, 10);
            color_panel_index[15].icon_color_path = " ";

            color_panel_index[16].color = Color.FromArgb(244, 32, 0, 0);
            color_panel_index[16].icon_color_path = " ";

            color_panel_index[17].color = Color.FromArgb(244, 255, 0, 0);
            color_panel_index[17].icon_color_path = " ";
        }

        public void init_basic_info(string[] road_name, int[] road_feature_id, uint[] road_degree, uint[] road_opening, uint[] road_channels, uint[] road_type, uint[] road_suface_struct, string layer_name)
        {
            int i = 0;
            int count = road_name.Length;
            gis_road_basic_information_unit_index = new gis_road_basic_information_unit[count];
            for (i = 0; i < count; i++)
            {
                gis_road_basic_information_unit_index[i] = new gis_road_basic_information_unit();
                gis_road_basic_information_unit_index[i].road_name = road_name[i];
                gis_road_basic_information_unit_index[i].road_feature_id = road_feature_id[i];
                gis_road_basic_information_unit_index[i].road_channels = road_channels[i];
                gis_road_basic_information_unit_index[i].road_degree = road_degree[i];
                gis_road_basic_information_unit_index[i].road_opening = road_opening[i];
                gis_road_basic_information_unit_index[i].road_type = road_type[i];
                gis_road_basic_information_unit_index[i].road_suface_struct = road_suface_struct[i];
            }
            this.layer_name = layer_name;
            road_gis_find_features_first(layer_name);
        }

        void p_PropertyChanged(object sender, EventArgs e)//事件的处理函数 
        {
            this.Invalidate();
        }
        private void label_info_Load(object sender, EventArgs e)
        {
            //  start_times();
        }
        void set_lable()
        {
            string ce = "\n\n";
            string ca = "";
            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
                ca = ca + gis_road_basic_information_unit_index[i].road_name + ce;
            }

            //  this.label_info.Caption = "广告位招租";// "通道：4 \n建设时间： 1998\n";
            //  this.label_info.Invalidate();
        }



        public void init_road_detection_db_data_by_tree_node( gis_road_basic_information_unit road_basic_information_unit_index,  
            gis_road_detection_type temp, string column_name,DataTable tb)
        {

            if (tb == null)
                return;
            int test_length = tb.Rows.Count;
            if (test_length <= 0)
                return;
            int kk = 0;
            road_basic_information_unit_index.road_detection_unit.road_length = test_length;
            if (road_basic_information_unit_index.road_detection_unit.gis_road_detection_type_arr != null)
            {    
                if (temp != null)
                {
                    temp.road_seg_value = new double[test_length];
                    foreach (DataRow dr in tb.Rows)
                    {
                        double va = Convert.ToDouble(dr[0]);
                        //double va = Convert.ToDouble((string)dr[0]);// (String.Format("{0,15}", dr[0]));
                        temp.road_seg_value[kk] = va;
                        kk++;
                    }
                }

            }

                ///////////////////////////////
                // gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit;
                if (road_basic_information_unit_index.road_detection_unit.ref_road_draw_unit != 0)
                    uninit_road_set(ref road_basic_information_unit_index.road_detection_unit.ref_road_draw_unit);
                init_road_set(ref road_basic_information_unit_index.road_detection_unit.ref_road_draw_unit);

                double[] pointss = new double[road_basic_information_unit_index.points.Length << 1];
                for (int ik = 0; ik < road_basic_information_unit_index.points.Length; ik++)
                {
                    pointss[ik << 1] = road_basic_information_unit_index.points[ik].X;
                    pointss[(ik << 1) + 1] = road_basic_information_unit_index.points[ik].Y;
                }
                do_equalization(road_basic_information_unit_index.road_detection_unit.ref_road_draw_unit,
                    pointss, road_basic_information_unit_index.points.Length,
                    road_basic_information_unit_index.road_detection_unit.road_length);//158 公里是157段



                int[] m_seg = new int[road_basic_information_unit_index.road_detection_unit.road_length << 1];
                uint[] color = new uint[6];
                uint[] colors = new uint[road_basic_information_unit_index.road_detection_unit.road_length];

                color[0] = 0x4fff5080; // 
                color[1] = 0x4f00ff0f; // 
                color[2] = 0x4fffffff; // 
                color[3] = 0x4f20f0f0; // 
                color[4] = 0x4f000000; // 
                color[5] = 0x4f0000ff; // 

                for (int iii = 0; iii < road_basic_information_unit_index.road_detection_unit.road_length; iii++)
                {
                    m_seg[(iii << 1)] = iii;
                    m_seg[(iii << 1) + 1] = iii + 1;
                    colors[iii] = color[iii % 6];
                }

                set_road_seg(road_basic_information_unit_index.road_detection_unit.ref_road_draw_unit,
                    m_seg, colors,
                    road_basic_information_unit_index.road_detection_unit.road_length);
                //////////////////////////////////////////////////////////////////////////////////////
           
        }

        public void init_road_detection_db_data()
        {
            int test_length = 10;
            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
            {
                gis_road_basic_information_unit_index[i].road_detection_unit.road_length = test_length;
                if (gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr != null)
                {
                    for (int j = 0; j < gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr.Count; j++)
                    {
                        gis_road_detection_type road_type =
                            (gis_road_detection_type)gis_road_basic_information_unit_index[i].road_detection_unit.gis_road_detection_type_arr[j];
                        if (road_type != null)
                        {
                            road_type.road_seg_value = new double[test_length];
                            for (int k = 0; k < test_length; k++)
                            {
                                road_type.road_seg_value[k] = 0.4;
                            }
                        }
                    }
                }
                ///////////////////////////////
                // gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit;
                if (gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit != 0)
                    uninit_road_set(ref gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit);
                init_road_set(ref gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit);

                double[] pointss = new double[gis_road_basic_information_unit_index[i].points.Length << 1];
                for (int ik = 0; ik < gis_road_basic_information_unit_index[i].points.Length; ik++)
                {
                    pointss[ik << 1] = gis_road_basic_information_unit_index[i].points[ik].X;
                    pointss[(ik << 1) + 1] = gis_road_basic_information_unit_index[i].points[ik].Y;
                }
                do_equalization(gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit,
                    pointss, gis_road_basic_information_unit_index[i].points.Length,
                    gis_road_basic_information_unit_index[i].road_detection_unit.road_length);//158 公里是157段



                int[] m_seg = new int[gis_road_basic_information_unit_index[i].road_detection_unit.road_length << 1];
                uint[] color = new uint[6];
                uint[] colors = new uint[gis_road_basic_information_unit_index[i].road_detection_unit.road_length];

                color[0] = 0x4fff5080; // 0x1303ff0f
                color[1] = 0x4f00ff0f; // 0x1303ff0f
                color[2] = 0x4fffffff; // 0x1303ff0f
                color[3] = 0x4f20f0f0; // 0x1303ff0f
                color[4] = 0x4f000000; // 0x1303ff0f
                color[5] = 0x4f0000ff; // 0x1303ff0f

                for (int iii = 0; iii < gis_road_basic_information_unit_index[i].road_detection_unit.road_length; iii++)
                {
                    m_seg[(iii << 1)] = iii;
                    m_seg[(iii << 1) + 1] = iii + 1;
                    colors[iii] = color[iii % 6];
                }

                set_road_seg(gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit,
                    m_seg, colors,
                    gis_road_basic_information_unit_index[i].road_detection_unit.road_length);
                //////////////////////////////////////////////////////////////////////////////////////
            }
        }
        private void init_other_component()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_gis_road));
            myImageList = new ImageList();
            myImageList.Images.Add(((System.Drawing.Image)(resources.GetObject("ic_card_morearrow_pressed"))));
            treeView_road_info.ImageList = myImageList;
            img_gaoguan_logo_point = new Point(10, 10);
            img_gaoguan_logo = ((System.Drawing.Image)(resources.GetObject("logo")));
            // treeView_road_info.SelectedImageIndex = 2;
        }
        public Form_gis_road()
        {
            init_color_panel();
            InitializeComponent();
            db_get_road_info();
            init_other_component();
            init_year();
            init_road_detaction_structure();
            init_road_detaction_structure1();
            init_road_detection_db_data();
            create_temp_layer(g_temp_layer_string);
            init_google_earth();
            //draw_line_with_mapx(new PointF(100, 100), new PointF(300, 200),0x123456,6);
            // 
            // TODO: 在 InitializeComponent 调用后添加任何构造函数代码 
            // 

            this.zoom = axMap1.Zoom; //取得初始化时的地图比例 
            this.centerx = axMap1.CenterX;
            this.centery = axMap1.CenterY; //取得初始化时的地图坐标,供刷新时使用 
            axMap1.CreateCustomTool(10, MapXLib.ToolTypeConstants.miToolTypePoly, MapXLib.CursorConstants.miCrossCursor, null, null, false);
            axMap1.CreateCustomTool(11, MapXLib.ToolTypeConstants.miToolTypePolygon, MapXLib.CursorConstants.miCrossCursor, null, null, false);
            axMap1.CurrentTool = MapXLib.ToolConstants.miPanTool;//漫游地图
            double x = axMap1.Zoom;
            x = x * 8.0 / 10.0;
            axMap1.Zoom = x;
           set_lable();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // axMap1.CurrentTool = MapXLib.ToolConstants.miZoomOutTool;
            // mapMain.CurrentTool = MapXLib.ToolConstants.miZoomInTool;

            axMap1.CurrentTool = MapXLib.ToolConstants.miZoomInTool;
        }

        private void axMap1_PolyToolUsed(object sender, AxMapXLib.CMapXEvents_PolyToolUsedEvent e)
        {
            if (e.toolNum == 10)//测量距离
            {
                MapXLib.Points pts = (MapXLib.Points)e.points;
                MapXLib.Point pt1, pt2;
                double dissum = 0.0;
                //计算顺序两个点距离,累计得到总距离
                // pts = (MapXLib.PointsClass)e.points;
                // dis = axMap1.Distance(pts._Item(pts.Count - 1).X, pts._Item(pts.Count - 1).Y, pts._Item(pts.Count).X, pts._Item(pts.Count).Y);
                for (int i = 1; i < pts.Count; i++)
                {
                    // double d = 0.0;
                    double d = 0.0;
                    pt1 = pts._Item(i);

                    pt2 = pts._Item(i + 1);
                    //d = axMap1.Distance(pts._Item(pts.Count - 1).X, pts._Item(pts.Count - 1).Y, pts._Item(pts.Count).X, pts._Item(pts.Count).Y);
                    d = axMap1.Distance(pt1.X, pt1.Y, pt2.X, pt2.Y);// mapMain.Distance(pt1.X, pt1.Y, pt2.X, pt2.Y);
                    dissum += d;
                }
            }
            else if (e.toolNum == 11)//面积
            {
                MapXLib.Points pts = (MapXLib.Points)e.points;
                MapXLib.FeatureFactory dd = axMap1.FeatureFactory;
                MapXLib.Style style = axMap1.DefaultStyle;
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = MapXLib.ToolConstants.miZoomOutTool;
        }

        private void Form_gis_road_Load(object sender, EventArgs e)
        {
            axMap1.CreateCustomTool(100, MapXLib.ToolTypeConstants.miToolTypePoly, MapXLib.CursorConstants.miCrossCursor, null, null, null);


        }

        private void button3_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = (MapXLib.ToolConstants)10;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            MapXLib.LayerInfo liinfo;
            liinfo = new MapXLib.LayerInfoClass();
            MapXLib.Fields flds = new MapXLib.FieldsClass();

            liinfo.Type = MapXLib.LayerInfoTypeConstants.miLayerInfoTypeTab;
            string sfile = openFile.InitialDirectory;
            string path = "C:\\Documents and Settings\\Administrator\\桌面\\Maps11";
            // liinfo.AddParameter("openFile.FileName");
            liinfo.AddParameter("Visible", true);
            liinfo.AddParameter("AutoCreateDataset", true);
            liinfo.AddParameter("datasetname", "layername");
            liinfo.AddParameter("filespec", path + "\\WORLD.TAB");
            flds.Add("Country", "Country", MapXLib.AggregationFunctionConstants.miAggregationSum, MapXLib.FieldTypeConstants.miTypeString);
            flds.Add("Capital", "Capital", MapXLib.AggregationFunctionConstants.miAggregationSum, MapXLib.FieldTypeConstants.miTypeString);
            flds.Add("Contient", "Contient", MapXLib.AggregationFunctionConstants.miAggregationSum, MapXLib.FieldTypeConstants.miTypeString);
            // dfs = axMap1.DataSets.Add(MapXLib.DatasetTypeConstants.miDataSetLayer,   , 0, 0, 0, flds, false);
            axMap1.Layers.Add(liinfo, 0);

            // the = axMap1.DataSets.Add(MapXLib.DatasetTypeConstants.miDataSetLayer);   
            axMap1.Layers.LayersDlg("", "");

        }
        private void axMap1_MapInitialized(object sender, EventArgs e)
        {
            init_gis_map();
            // db_get_road_info();

        }
        private void axMap1_MouseUpEvent(object sender, AxMapXLib.CMapXEvents_MouseUpEvent e)
        {
            if (e.button == 1)
            {

                MapXLib.Layer layer;

                try
                {
                    layer = axMap1.Layers._Item(this.layer_name);
                }
                catch
                {
                    return;
                }
                int ccc = layer.Selection._Item(1).FeatureID;
                string aa = Convert.ToString(ccc);

             //   MessageBox.Show("feature id: " + aa, "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            else if (e.button == 2)
            {
                // show contextmenuStrip
            }
            //   axMap1.PropertyPage();
        }
        public void show_road_basic_info(int draw_type, int draw_type_color_index)
        {
            this.draw_type = draw_type;
            this.draw_type_color_index = draw_type_color_index;


        }


        void draw_path( uint[] colors, uint ptest)
        {

            PointF[] new_points = null;
            int seg_number = get_road_seg_number(ptest);
            if (colors.Length < seg_number - 1)
                return;
            uint color = 0;
            int from = 0;
            int to = 0;
            double x = 0;
            double y = 0;
            uint colorss = 0;
            int ij = from;
            int count = to - from;
            int j = 0;

            for (int i = 0; i < seg_number; i++)
            {

                get_road_seg_by_index(ptest, i, ref from, ref to, ref color);
                color = colors[i];
                count = to - from + 1;
                new_points = new PointF[count + 1];
                for (int ci = 0; ci < count + 1; ci++)
                {
                    new_points[ci] = new PointF();
                }

                ij = from;
                for (j = 0; j < count; j++)
                {
                    get_road_seg_point_by_index(ptest, ij << 1, ref  x, ref  y, ref colorss);

                    new_points[j].X = (int)x;
                    new_points[j].Y = (int)y;
                    ij++;
                }
                ij--;
                get_road_seg_point_by_index(ptest, (ij << 1) + 1, ref  x, ref  y, ref colorss);

                new_points[j].X = ((int)x);
                new_points[j].Y = ((int)y);

                draw_line_with_mapx(new_points, color, 6);

            }

        }






        private void DrawGisMapssTest( string type)
        {
            int pan_width = 4;
            int A = 255;
            int R = 233;
            int G = 13;
            int B = 75;
            road_type_color_value col = null;
            if (type != null)
            {
                switch (draw_type)
                {
                    case 0: // road opening
                        {

                            for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                            {
                                if (!gis_road_basic_information_unit_index[i].is_draw)
                                    continue;
                                if (gis_road_basic_information_unit_index[i].points != null)
                                {

                                    GraphicsPath pat = new GraphicsPath();
                                    pat.AddLines(gis_road_basic_information_unit_index[i].points);

                                    gis_road_basic_information_unit_index[i].basic_information_unit_types.TryGetValue(type, out col);
                                   
                                    draw_line_with_mapx(gis_road_basic_information_unit_index[i].points, col.color, 6);
          
                              

                                }
                            }


                            break;
                        }
                    case 5: //road_suface_struct
                        {
                            {
                                for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                                {
                                    if (!gis_road_basic_information_unit_index[i].is_draw)
                                        continue;
                                    if (gis_road_basic_information_unit_index[i].road_detection_unit.road_seg_color == null)
                                        return;

                                    draw_path( gis_road_basic_information_unit_index[i].road_detection_unit.road_seg_color,
                                    gis_road_basic_information_unit_index[i].road_detection_unit.ref_road_draw_unit);
                                }

                            }
                            break;
                        }
                    default:
                        break;

                }
            }


        }




        private void draw_logo(int dcs)
        {
            return;
            Graphics g = Graphics.FromHdc((IntPtr)dcs);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawImage(img_gaoguan_logo, img_gaoguan_logo_point);

        }


        private void axMap1_DrawUserLayerEvent(object sender, AxMapXLib.CMapXEvents_DrawUserLayerEvent e)
        {
            //      System.Threading.Mutex run = new System.Threading.Mutex(true, "jiaao_test", out runone); 
            //  if(is_mouse_down==0)
            //   road_gis_find_features();
            if (gis_road_basic_information_unit_index != null)
            {
                road_gis_find_features(layer_name);
                // DrawGisMapss(e.hOutputDC);
                draw_logo(e.hOutputDC);
              //  DrawGisMapssTest(e.hOutputDC, draw_type_string);
            }
        }

        private void axMap1_DDrawEvent(object sender, AxMapXLib.CMapXEvents_MapDrawEvent e)
        {

        }
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = (MapXLib.ToolConstants)11;
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = (MapXLib.ToolConstants)10;
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = MapXLib.ToolConstants.miZoomOutTool;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = MapXLib.ToolConstants.miZoomInTool;
        }


        private void init_gis_map()
        {
            axMap1.GeoSet = "C:\\gst_map3\\gg_road.gst";
            axMap1.TitleText = "";
            MapXLib.Layer ly;
       //     ly = axMap1.Layers.AddUserDrawLayer("ss", 1);
       //     this.axMap1.DrawUserLayer += new AxMapXLib.CMapXEvents_DrawUserLayerEventHandler(this.axMap1_DrawUserLayerEvent);
            return;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            init_gis_map();
            return;
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            axMap1.PropertyPage();
        }

        private void axMap1_MapViewChanged(object sender, EventArgs e)
        {

        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = MapXLib.ToolConstants.miPanTool;//漫游地图
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = MapXLib.ToolConstants.miSymbolTool;//添加符号
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            axMap1.DisplayCoordSys.PickCoordSys(null, null);// 改变投影
            axMap1.NumericCoordSys = axMap1.DisplayCoordSys;
        }

        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            //axMap1.layers.layersdlg(null, null);
            axMap1.Layers.LayersDlg(null, null);   //图层控制面板
        }

        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            // axmap1.currenttool = mapxlib.toolconstants.mirectselecttool;
            axMap1.CurrentTool = MapXLib.ToolConstants.miRectSelectTool;   //矩形选择
            //menuitem5.radiocheck = true;
            //toolStripButton11_Click.radiocheck = true;  
        }

        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            axMap1.DisplayCoordSys.PickCoordSys(null, null);
            axMap1.NumericCoordSys = axMap1.DisplayCoordSys;

        }
        static int draw_types = 0;
        private void toolStripButton13_Click(object sender, EventArgs e)
        {
            //show_road_basic_info(draw_types, 7);
            show_road_basic_info(draw_types, -1);
            draw_types++;
            draw_types = draw_types % 5;
        }

        private void axMap1_MouseWheelEvent(object sender, AxMapXLib.CMapXEvents_MouseWheelEvent e)
        {
         //   if (e.zDelta > 0)
         //   {
                //axMap1.CenterX;     
         //   }
        }

        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            axMap1.CurrentTool = MapXLib.ToolConstants.miSelectTool;
        }

        MapXLib.Feature[] ftr = null; // dude, you threathen me !!!!!

        private void road_gis_find_features_first(string layer_name)
        {
            this.layer_name = layer_name;// new string(layer_name.ToString());
            axMap1.NumericCoordSys = axMap1.DisplayCoordSys;
            MapXLib.Layer layer;

            try
            {
                layer = axMap1.Layers._Item(this.layer_name);
            }
            catch
            {
                return;
            }
            try
            {
                //  MapXLib.Feature ftr;

                double MapCoordX = 0;
                double MapCoordY = 0;
                float floatX = 0;
                float floatY = 0;
                if (ftr == null)
                {
                    ftr = new MapXLib.Feature[gis_road_basic_information_unit_index.Length];

                }
                for (int j = 0; j < gis_road_basic_information_unit_index.Length; j++)
                {
                    try
                    {
                        if (ftr[j] == null)
                        {
                            // ftr[j] = new MapXLib.Feature();
                            ftr[j] = layer.GetFeatureByID(gis_road_basic_information_unit_index[j].road_feature_id);
                            string aaa = ftr[j].FeatureKey;
                        }
                    }
                    catch
                    {
                        continue;
                    }
                    //  if (ftr.Type.ToString().CompareTo(MapXLib.FeatureTypeConstants.miFeatureTypeLine.ToString()) == 0)
                    {
                        MapXLib.Points pts;//=new MapXLib.PointsClass();
                        try
                        {
                            int few = ftr[j].Parts.Count;
                            pts = ftr[j].Parts[1];
                            //gis_road_basic_information_unit_index[j].points = null;
                            gis_road_basic_information_unit_index[j].points = new PointF[pts.Count];
                            int t = pts.Count + 1;
                            for (int i = 1; i < t; i++)
                            {
                                MapCoordX = pts.get_X(i);
                                MapCoordY = pts.get_Y(i);
                                axMap1.ConvertCoord(ref  floatX, ref floatY, ref MapCoordX, ref  MapCoordY, MapXLib.ConversionConstants.miMapToScreen);
                                // gis_road_basic_information_unit_index[j].points[i - 1] = new Point();
                                gis_road_basic_information_unit_index[j].points[i - 1].X = (floatX);
                                gis_road_basic_information_unit_index[j].points[i - 1].Y = (floatY);
                            }
                        }
                        catch
                        {
                            return;
                        }

                    }
                }
              //  layer.Visible = false;
            }
            catch
            {
                return;
            }
            //     layer.Visible = false;

        }

        public class fPoint
        {
            public double X;
            public double Y;
        };
        private void road_gis_find_features(string layer_name)
        {
            this.layer_name = layer_name;// new string(layer_name.ToString());
            axMap1.NumericCoordSys = axMap1.DisplayCoordSys;
            MapXLib.Layer layer;

            try
            {
                layer = axMap1.Layers._Item(this.layer_name);
            }
            catch
            {
                return;
            }
            try
            {
                //  MapXLib.Feature ftr;

                double MapCoordX = 0;
                double MapCoordY = 0;
                float floatX = 0;
                float floatY = 0;

                for (int j = 0; j < gis_road_basic_information_unit_index.Length; j++)
                {
                    try
                    {
                        if (ftr[j] == null)
                            ftr[j] = layer.GetFeatureByID(gis_road_basic_information_unit_index[j].road_feature_id);
                    }
                    catch
                    {
                        continue;
                    }
                    //  if (ftr.Type.ToString().CompareTo(MapXLib.FeatureTypeConstants.miFeatureTypeLine.ToString()) == 0)
                    {
                        MapXLib.Points pts;//=new MapXLib.PointsClass();
                        try
                        {
                            int few = ftr[j].Parts.Count;
                            pts = ftr[j].Parts[1];
                            //gis_road_basic_information_unit_index[j].points = null;
                            gis_road_basic_information_unit_index[j].points = new PointF[pts.Count];
                            int t = pts.Count + 1;
                            for (int i = 1; i < t; i++)
                            {
                                MapCoordX = pts.get_X(i);
                                MapCoordY = pts.get_Y(i);
                                axMap1.ConvertCoord(ref  floatX, ref floatY, ref MapCoordX, ref  MapCoordY, MapXLib.ConversionConstants.miMapToScreen);
                                // gis_road_basic_information_unit_index[j].points[i - 1] = new Point();
                                gis_road_basic_information_unit_index[j].points[i - 1].X = (floatX);
                                gis_road_basic_information_unit_index[j].points[i - 1].Y = (floatY);
                            }
                        }
                        catch
                        {
                            return;
                        }
                    }
                }
                //layer.Visible = false;
            }
            catch
            {
                return;
            }
            init_road_detection_db_data();
            //     layer.Visible = false;

        }
        private void Form_gis_road_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void Form_gis_road_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void resize_treelist(int wid)
        {
            int sx = wid - g_list_wid;
            int s_width = 20;
            this.axMap1.SetBounds(wid + 8, 4, this.Width - wid - 32, this.Height - 40 );
        //  this.axMap1.Update();
        //  this.axMap1.Invalidate();
        //  this.treeView_road_info.SetBounds(5, axMap1.Bounds.Y + 5, wid, axMap1.Bounds.Height - 3);
            this.webBrowser_google_earth.SetBounds(axMap1.Bounds.X, axMap1.Bounds.Y, axMap1.Bounds.Width, axMap1.Bounds.Height);

      //      this.treeView_road_info.SetBounds(5, axMap1.Bounds.Y, 4, axMap1.Bounds.Height - 3);
            this.road_select_list.SetBounds(5, axMap1.Bounds.Y+20, wid, g_list_road_height - 5);
            button_indent2.SetBounds(10,axMap1.Bounds.Y,
                 20, 20);
            google_earth.SetBounds(wid + 8 - 80, axMap1.Bounds.Y,
                 20, 20);
            this.treeListGis.SetBounds(5, axMap1.Bounds.Y + g_list_road_height+20, wid, axMap1.Bounds.Height - g_list_road_height - 30);
         
            
            comboBoxYear.SetBounds(sx+s_width-320, axMap1.Bounds.Y + axMap1.Bounds.Height - 20, 60, 30);
            checkBoxHigh.SetBounds(sx + s_width + 70-320, axMap1.Bounds.Y + axMap1.Bounds.Height - 20, 40, 30);
            checkBoxLow.SetBounds(sx + s_width + 70 + 50-320, axMap1.Bounds.Y + axMap1.Bounds.Height - 20, 40, 30);
            button_indent.SetBounds(10, axMap1.Bounds.Y + axMap1.Bounds.Height - 10,
                20, 20);
        }
        private int g_list_wid1=340;
        private int g_list_wid = 300;
        private int g_list_road_height = 0;
        private void Form_gis_road_SizeChanged(object sender, EventArgs e)
        {
           resize_treelist(g_list_wid); 
        }
        
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        }

        #region

        const string utility_dll_path = "c://gst_map3//road_gis_utility_set.dll";

        #endregion

        public void theout(object source, System.Timers.ElapsedEventArgs e)
        {
            return;
            earth_action_span_time = 0;
            aTimer.Enabled = false;
        }

        void settime(int time)
        {
            return;
            earth_action_span_time = 1;
            aTimer.Elapsed += new ElapsedEventHandler(theout);
            aTimer.Interval = time;
            //    aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private int earth_action_span_time = 0;
        System.Timers.Timer aTimer = new System.Timers.Timer();
        private int ki = 0;

        private void do_google_earth_act(string label, double x, double y, string link, string info, string link_url)
        {
            do_google_script( label,  x,  y,  link,  info,  link_url);
            return;
         
        }

        private void do_google_script(string label, double x, double y, string link, string info, string link_url)
        {

            if (is_navigate == 0)
                return;
            IHTMLWindow2 win = (IHTMLWindow2)webBrowser_google_earth.Document.Window.DomWindow;
            webBrowser_google_earth.Document.InvokeScript("setposition", new object[] { x, y });
            // object i = webBrowser_google_earth.Document.InvokeScript("setposition", new object[] { 1, 2 });
            //int sum = Convert.ToInt32(i);

            string aaa = "<a href=\"http://wuxianghui.100free.com/\"><img width=140 height=60 src=";
            aaa = aaa + link;
            aaa = aaa + "/></a><br /><a href=\"";//\"http://www.baidu.com\">";
            aaa = aaa + link_url;
            aaa = aaa + "\">";
            //  aaa=aaa+info;
            aaa = aaa + "</a>";
            webBrowser_google_earth.Document.InvokeScript("set_balloon", new object[] { x, y, aaa });
            return;
            //  mshtml.IHTMLDocument2 currentDoc = (mshtml.IHTMLDocument2)webBrowser_google_earth.Document.DomDocument;
            //     mshtml.IHTMLWindow2 win = (mshtml.IHTMLWindow2)currentDoc.;
            //  win.execScript("road_gis_utility_set_boollon_tip(1312.0,3131.0,'ffff')", "javascript");//调用函数F1

            //  webBrowser_google_earth.Url =  new Uri("js.html");
      //      road_gis_utility_set_boollon_tip(doc, x, y, aaa.ToCharArray());
        }


        /// <summary>
        /// //////////////////////////////////// below is for db operation ////////////
        /// </summary>
        /// 
        void get_archive_date_by_road_name(string road_name)
        {

        }
        public void init_basic_road_info1(string[] road_name, Dictionary<string, int> road_name_featureid, string layer_name)
        {
            int i = 0;
            int count = road_name.Length;
            int fid = 0;
            gis_road_basic_information_unit_index = new gis_road_basic_information_unit[count];
            for (i = 0; i < count; i++)
            {
                gis_road_basic_information_unit_index[i] = new gis_road_basic_information_unit();
                gis_road_basic_information_unit_index[i].road_name = road_name[i];
                road_name_featureid.TryGetValue(gis_road_basic_information_unit_index[i].road_name, out fid);
                gis_road_basic_information_unit_index[i].road_feature_id = fid;

                ///////////////////////////////////////////////////////////////////////////////////
                
                DataSql ds = new DataSql();
                DataTable tr = ds.getGuiDangRiQi(gis_road_basic_information_unit_index[i].road_name);
                if (tr != null)
                {
                    gis_road_basic_information_unit_index[i].archive_date = new ArrayList();
                    foreach (DataRow tdr in tr.Rows)
                    {
                        gis_road_basic_information_unit_index[i].archive_date.Add(Convert.ToDateTime(tdr[0]));
                    }
                }

              
                ///////////////////////////////////////////////////////////////////////////////////


            }
            this.layer_name = layer_name;
            road_gis_find_features_first(layer_name);
        }


        public void init_basic_road_info(string[] road_name, int[] road_feature_id, string layer_name)
        {
            int i = 0;
            int count = road_name.Length;

            gis_road_basic_information_unit_index = new gis_road_basic_information_unit[count];
            for (i = 0; i < count; i++)
            {
                gis_road_basic_information_unit_index[i] = new gis_road_basic_information_unit();
                gis_road_basic_information_unit_index[i].road_name = road_name[i];
                gis_road_basic_information_unit_index[i].road_feature_id = road_feature_id[i];
                gis_road_basic_information_unit_index[i].is_draw = false;
            }
            this.layer_name = layer_name;
            road_gis_find_features_first(layer_name);
        }
        private void db_get_road_name(OleDbConnection conn)
        {
            string cz_road_name = "select b.路线名称 from 管辖路段信息表 a,路线信息表 b where a.单位编号=65 and a.路线编号=b.ID";
            ArrayList ar = new ArrayList();

            OleDbCommand cmd = new OleDbCommand(cz_road_name, conn);

            cmd.ExecuteNonQuery();

            int[] road_feature_id = new int[11];
            road_feature_id[0] = 7;
            road_feature_id[1] = 11;
            road_feature_id[2] = 12;
            road_feature_id[3] = 9;
            road_feature_id[4] = 10;
            road_feature_id[5] = 13;
            road_feature_id[6] = 8;
            road_feature_id[7] = 18;
            road_feature_id[8] = 15;
            road_feature_id[9] = 17;
            road_feature_id[10] = 16;

            string[] road_names = new string[11];

            road_names[0] = "机场高速公路";
            road_names[1] = "宁连高速公路南京段";
            road_names[2] = "宁连高速公路淮安段";
            road_names[3] = "宁连高速公路淮安北环段";
            road_names[4] = "宁连高速公路淮连段";
            road_names[5] = "宁洛高速公路江苏段";
            road_names[6] = "宁连一级公路南京段";
            road_names[7] = "宁连一级公路淮安段";
            road_names[8] = "宁高高速公路";
            road_names[9] = "通启高速公路";
            road_names[10] = "宁通高速公路";

            Dictionary<string, int> road_name_featureid = new Dictionary<string, int>();
            for (int k = 0; k < road_names.Length; k++)
            {
                road_name_featureid.Add(road_names[k], road_feature_id[k]);
            }
            
            //读取数据库内容 
            OleDbDataAdapter adapter = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adapter.Fill(ds);

            if (ds.Tables[0].Rows.Count <= 0)
            {
                MessageBox.Show("没有查询到符合条件的记录！");
            }
            else
            {
                for (int ii = 0; ii < ds.Tables[0].Rows.Count; ii++)
                {
                    string ming = ds.Tables[0].Rows[ii]["路线名称"].ToString();
                    ar.Add(ming);

                }
                string[] te = new string[ar.Count];
                for (int ki = 0; ki < ar.Count; ki++)
                {
                    te[ki] = (string)ar[ki];
                }
                init_basic_road_info1(te, road_name_featureid, "高管中心公路");//高管中心公路 //highway_line
            }
        }
        public class road_type_color_value
        {
            public string table_name;
            public string field_name;
            public uint color;
            public double value;
        };
        private void db_get_road_info()
        {
            OleDbConnection conn;
            string link = "Provider=SQLOLEDB;Data Source=wuxianghui\\SQLEXPRESS;Initial Catalog=HWSFTHPMS;uid=wuxianghui;pwd=wuxianghui";
            conn = new OleDbConnection(link);
            conn.Open();
            db_get_road_name(conn);
            int i;
            //cz = "select* from 路线信息表 where ID=54";
            string cz_tong_che_shi_jian = "select 通车时间,路面等级,路线名称,路面类型 from jianshexinxi a,路线信息表 b,管辖路段信息表 c,lumiandengji d,lumianleixing e where a.路线ID=b.ID and c.单位编号=65 and c.路线编号=b.ID and d.路线ID=b.ID and e.路线ID=b.ID";
            //管辖路段信息表 a,路线信息表 b where a.单位编号=65
            OleDbCommand cmd = new OleDbCommand(cz_tong_che_shi_jian, conn);

            cmd.ExecuteNonQuery();


            //读取数据库内容 
            OleDbDataAdapter adapter = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
        

            if (ds.Tables[0].Rows.Count <= 0)
            {
                MessageBox.Show("没有查询到符合条件的记录！");
            }
            else
            {
                for (int ii = 0; ii < ds.Tables[0].Rows.Count; ii++)
                {
                    string ming = ds.Tables[0].Rows[ii]["路线名称"].ToString();
                    string timess = ds.Tables[0].Rows[ii]["通车时间"].ToString();
                    string degrees = ds.Tables[0].Rows[ii]["路面等级"].ToString();
                    string lumianleixing = ds.Tables[0].Rows[ii]["路面类型"].ToString();

                    for (i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                    {
                        if (gis_road_basic_information_unit_index[i].road_name.CompareTo(ming) == 0)
                        {
                            string[] ar = timess.Split('-');
                            if (ar.Length > 2)
                            {
                                string pp = ar[0];
                                {
                                    string ppp = pp.Split(' ')[0];
                                    int ye = Convert.ToInt32(ppp);
                                    gis_road_basic_information_unit_index[i].basic_information_unit_types.Remove("通车时间");
                                    road_type_color_value temp = new road_type_color_value();
                                    temp.color = 0xff4fef34;
                                    temp.value = ye;
                                    gis_road_basic_information_unit_index[i].basic_information_unit_types.Add("通车时间", temp);
                                }
                            }

                            /// 路面等级

                            {
                                double de = 0;
                                if (degrees.CompareTo("高速公路") == 0)
                                {
                                    de = 1998.0;
                                }
                                else
                                {
                                    de = 2002;
                                }
// from 
                                gis_road_basic_information_unit_index[i].basic_information_unit_types.Remove("路面等级");

                                road_type_color_value temp = new road_type_color_value();
                                temp.color = 0xff4fef34;
                                temp.value = de;

                                gis_road_basic_information_unit_index[i].basic_information_unit_types.Add("路面等级", temp);

                            }
                            //////////////////
                            /// 路面类型

                            {
                                double de = 0;
                                if (lumianleixing.CompareTo("沥青混凝土") == 0)
                                {

                                    de = 4;
                                }
                                else
                                {

                                    de = 6;
                                }


                                gis_road_basic_information_unit_index[i].basic_information_unit_types.Remove("路面类型");

                                road_type_color_value temp = new road_type_color_value();
                                temp.color = 0xff4fef34;
                                temp.value = de;

                                gis_road_basic_information_unit_index[i].basic_information_unit_types.Add("路面类型", temp);

                            }
                            //////////////////
                        }
                    }

                }

            }
            conn.Close();
            conn.Dispose();
        }
        static long atomic_do_basic_info_action_counter = 1;
        System.Threading.Thread th_do_basic_info_action;//= new System.Threading.Thread(f1);
        static void f_do_basic_info_action()
        {
            //  Interlocked.Increment(ref atomic_do_basic_info_action_counter);
            current_form.do_basic_info_action(do_basic_info_action_node);
            Interlocked.Decrement(ref atomic_do_basic_info_action_counter);
        }
        private static TreeNode do_basic_info_action_node;
        private static Form_gis_road current_form;

        [DllImport("user32.dll ", CharSet = CharSet.Unicode)]
        public static extern IntPtr PostMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);
        private void treeView_road_info_AfterCheck(object sender, TreeViewEventArgs e)
        {
            current_form = this;
            TreeNode pt = e.Node;
            if (pt != null && pt.Parent != null)
            {
                //  do_basic_info_action_node = pt.Parent;
                //  th_do_basic_info_action =
                //     new System.Threading.Thread(f_do_basic_info_action);
                //  th_do_basic_info_action.Start();
                do_basic_info_action(pt.Parent);
                //  PostMessage(this, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            }
        }

        private void treeListGis_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            if (e.Column.FieldName == "tuli")
            {
                //   e.Appearance.BackColor = SystemColors.ControlDark;

                //   Brush ellipseBrush =  Brushes.Red;
                //  Rectangle r = e.Bounds;
                //  e.Graphics.FillEllipse(ellipseBrush, r);

                //  e.Graphics.FillRectangle(ellipseBrush, r);
                if (e.Node.ParentNode != null)
                {
                    string pa = (string)e.Node.ParentNode.GetValue(0);
                    if (pa.CompareTo(_XUAN_ZE_LU_XIAN_) == 0)
                    {
                        return;
                    }
                }
                String cc = (String)e.Node.GetValue(1);

                if (cc != null)
                {
                    uint icolor = Convert.ToUInt32(cc, 10);
                    Color BackColor = Color.FromArgb(255, (int)(icolor & 0x00ff0000) >> 16,
                             (int)(icolor & 0x0000ff00) >> 8, (int)(icolor & 0x000000ff));

                    //        Brush brush = new System.Drawing.Drawing2D.LinearGradientBrush(e.Bounds, Color.Red, Color.Red, 90);

                    e.Appearance.ForeColor = BackColor;
                    e.Appearance.BackColor = BackColor;


                    //   Rectangle r = e.Bounds;
                    //     e.Graphics.FillEllipse(brush, r);

                }
                //     String aa = cc; 
                //     String bb=(String)e.Node.Tag;

                //     uint icolor = Convert.ToUInt32(bb, 10);

                //      Color BackColor = Color.FromArgb(255, (int)(icolor & 0x00ff0000) >> 16,
                //             (int)(icolor & 0x0000ff00) >> 8, (int)(icolor & 0x000000ff));
            }
        }

        private void treeListGis_CustomNodeCellEdit(object sender, GetCustomNodeCellEditEventArgs e)
        {
            if (e.Column.FieldName == "check")
            {
                if (e.Node.ParentNode != null)
                {
                    string pa = (string)e.Node.ParentNode.GetValue(0);
                    if (pa.CompareTo(_LU_XIAN_MING_CHENG_) == 0)
                    {
                        e.RepositoryItem = repositoryItemCheckEdit1;

                        return;
                    }
                }
                if (e.Node.HasChildren)
                    return;
                object obj = e.Node.GetValue(0);
                if (obj != null)
                {
                    e.RepositoryItem = repositoryItemCheckEdit1;

                }
            }
            else if (e.Column.FieldName == "tuli")
            {
                if (e.Node.ParentNode != null)
                {
                    string pa = (string)e.Node.ParentNode.GetValue(0);
                    if (pa.CompareTo(_XUAN_ZE_LU_XIAN_) == 0)
                    {
                        repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
                        repositoryItemComboBox1.AllowFocused = false;
                        repositoryItemComboBox1.AutoHeight = false;
                        repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});

                        repositoryItemComboBox1.Items.AddRange(new object[] {
                         "1990",
                         "1991",
                         "1992",
                         "1993",
                         "1994",
                         "1995",
                         "1996",
                         "1997"});
                        repositoryItemComboBox1.Name = "repositoryItemComboBox1";
                        e.RepositoryItem = repositoryItemComboBox1;
                        return;
                    }
                else
                {
                    object obj = e.Node.GetValue(0);
                    if (obj != null)
                    {
                        e.RepositoryItem = repositoryItemTextEdit1;
                        // e.RepositoryItem = repositoryItemImageEdit1;
                    }
                }
             }
            }
            return;
        }

        private void treeListGis_DoubleClick(object sender, EventArgs e)
        {


            if (treeListGis.FocusedNode == null)
                return;
            do_tree_click_action1(sender, (MouseEventArgs)e);

            return;
            TreeListNode tn;

            tn = treeListGis.FocusedNode;
            String cc = (string)tn.GetValue(1); ;

            if (tn.ParentNode != null)
            {
                String aaa = tn.ParentNode.GetValue(0).ToString();
                if (tn.ParentNode.GetValue(0).ToString().CompareTo("基础数据") == 0)
                {
                    if (tn.Nodes[0] != null || (tn.Nodes[0].Nodes.Count == 0))
                    {
                        MessageBox.Show(this, "leaf");
                    }
                }
            }
            return;
            if (cc != null)
            {
                uint icolor = Convert.ToUInt32(cc, 10);
                Color BackColor = Color.FromArgb(255, (int)(icolor & 0x00ff0000) >> 16,
                         (int)(icolor & 0x0000ff00) >> 8, (int)(icolor & 0x000000ff));

                //        Brush brush = new System.Drawing.Drawing2D.LinearGradientBrush(e.Bounds, Color.Red, Color.Red, 90);
                MessageBox.Show(this, BackColor.ToString());
                //   Rectangle r = e.Bounds;
                //     e.Graphics.FillEllipse(brush, r);
            }
            int i = 100;
        }

        private void treeListGis_AfterCollapse(object sender, NodeEventArgs e)
        {
            if (e.Node.Nodes[0].HasChildren == false)
            {
                

                    string pa = (string)e.Node.GetValue(0);
                    if (pa.CompareTo(_LU_XIAN_MING_CHENG_) == 0)
                    {
                        e.Node.Expanded = false;
                     //  e.Node.Visible = false;
                    }

                    else
                    {
                        e.Node.Expanded = true;
                    }

               
            }
            //    e.Node.Expanded = true;

        }

        void init_year()
        {
            comboBoxYear.Items.Add("2007");
            comboBoxYear.Items.Add("2008");
            comboBoxYear.Items.Add("2009");
            comboBoxYear.Items.Add("2010");
            comboBoxYear.Items.Add("2011");
            comboBoxYear.Items.Add("2012");
            
          //  checkBoxHigh.Select = false; ;
       //  checkBoxLow;
        }

        private void checkBoxHigh_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxLow.Checked = !checkBoxHigh.Checked;
        }

        private void checkBoxLow_CheckedChanged(object sender, EventArgs e)
        {
         checkBoxHigh.Checked = !checkBoxLow.Checked;
        }

        private void Form_gis_road_MouseClick(object sender, MouseEventArgs e)
        {
            if (treeListGis.FocusedNode == null)
                return;
            TreeListNode node_t = treeListGis.FocusedNode;
            TreeListNode p_node_t = node_t.ParentNode;

            if(p_node_t==null)
                return;
            if (e.Button == MouseButtons.Left)

            {

                if (p_node_t.GetValue(0).ToString().CompareTo(_LU_XIAN_MING_CHENG_) == 0)
                        {

                            node_t.SetValue(2,!((bool)node_t.GetValue(2)));

                            return;
                        }
            }
        }

        private void treeListGis_Click(object sender, EventArgs e)
        {
            if (treeListGis.FocusedNode == null)
                return;
            TreeListNode node_t = treeListGis.FocusedNode;
            TreeListNode p_node_t = node_t.ParentNode;
            TreeListNode c_node_t = null;
            if (p_node_t == null)
                return;

            if(!node_t.HasChildren)
            {
                if(node_t.GetValue(2)!=null)
                node_t.SetValue(2, !((bool)node_t.GetValue(2)));
            }

            if (p_node_t.GetValue(0).ToString().CompareTo(_LU_XIAN_MING_CHENG_) == 0)
            {
                    string r_n=(string)node_t.GetValue(0);
                    if(r_n==null)
                        return;
                    for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                    {
                        if (gis_road_basic_information_unit_index[i].road_name.CompareTo(r_n) == 0)
                        {
                            gis_road_basic_information_unit_index[i].is_draw = (bool)node_t.GetValue(2);
                            break;
                        }
                    }
                   return;
             }

        }

        private void create_temp_layer(String layer_name)
        {
            MapXLib.LayerInfoClass gpsInfo = new MapXLib.LayerInfoClass();
            MapXLib.FieldsClass newField1 = new MapXLib.FieldsClass();
            newField1.AddStringField("GeoName", 20, false);
            gpsInfo.Type = MapXLib.LayerInfoTypeConstants.miLayerInfoTypeTemp;
            gpsInfo.AddParameter("Name", "gpsLayer");
            gpsInfo.AddParameter("Fields", newField1);
            g_temp_layer = axMap1.Layers.Add(gpsInfo, 1);
            g_temp_layer.Editable = true;
            g_temp_layer.AutoLabel = true;
        }
        MapXLib.Layer g_temp_layer;//gpsLayer;
        String g_temp_layer_string="temp_layer";//gpsLayer;


        private void draw_line_with_mapx()
        {

            //高管中心公路
            // create_temp_layer(g_temp_layer_string);
            if (g_temp_layer == null)
                return;

            MapXLib.Layer lyr = g_temp_layer;// axMap1.Layers._Item(this.layer_name);

            MapXLib.Feature tempFea;//声明Feature变量
            MapXLib.Style tempStyle = new MapXLib.Style();//声明Style变量
            Double Xmin, Xmax, Ymin, Ymax;
            //  RegionPoint Pmin, Pmax;

            tempStyle.RegionPattern = MapXLib.FillPatternConstants.miPatternSolid;//设置Style的矩形内部填充样式
            tempStyle.RegionColor = 0x123456;
            tempStyle.RegionBorderWidth = 2;//设置Style的矩形边框宽度



            double MapX = 0;//定义x坐标变量
            double MapY = 0;//定义y坐标变量
            float ex = 0;//定义x坐标变量
            float ey = 0;//定义y坐标变量
            MapXLib.Points pts = new MapXLib.Points();

            ex = 100;
            ey = 100;
            axMap1.ConvertCoord(ref ex, ref ey, ref MapX, ref MapY, MapXLib.ConversionConstants.miScreenToMap);
            pts.AddXY(MapX, MapY, Missing.Value);

            ex = 200;
            ey = 200;
            axMap1.ConvertCoord(ref ex, ref ey, ref MapX, ref MapY, MapXLib.ConversionConstants.miScreenToMap);
            pts.AddXY(MapX, MapY, Missing.Value);

            ex = 400;
            ey = 150;
            axMap1.ConvertCoord(ref ex, ref ey, ref MapX, ref MapY, MapXLib.ConversionConstants.miScreenToMap);
            pts.AddXY(MapX, MapY, Missing.Value);



            tempFea = axMap1.FeatureFactory.CreateRegion(pts, tempStyle);
            lyr.AddFeature(tempFea, Missing.Value);//添加矩形边框
            lyr.Refresh();

        }

        private void remove_all_temp_feature()
        {
            if (g_temp_layer == null)
                return;
            foreach (MapXLib.Feature f in g_temp_layer.AllFeatures)
            {
                g_temp_layer.DeleteFeature(f);
            }
       
        }
        private void draw_line_with_mapx(PointF p1,PointF p2,uint color,int line_width)
    {

        //高管中心公路
       // create_temp_layer(g_temp_layer_string);
            if (g_temp_layer == null)
                return;
       
           MapXLib.Layer lyr = g_temp_layer;// axMap1.Layers._Item(this.layer_name);

           MapXLib.Feature tempFea;//声明Feature变量
           MapXLib.Style tempStyle = new MapXLib.Style();//声明Style变量

       //    tempStyle.RegionPattern = MapXLib.FillPatternConstants.miPatternSolid;//设置Style的矩形内部填充样式
       //    tempStyle.RegionColor = 0x123456;
       //    tempStyle.RegionBorderWidth = 2;//设置Style的矩形边框宽度

        //   tempStyle. = MapXLib.LineTypeConstants.miLineTypeSimple;//设置Style的矩形内部填充样式

           if (line_width > 6)
               line_width = 6;

           tempStyle.LineColor = color&0x00ffffff;
           tempStyle.LineWidth = (short)line_width;//设置Style的矩形边框宽度
            

           double MapX = 0;//定义x坐标变量
           double MapY = 0;//定义y坐标变量
           float ex = 0;//定义x坐标变量
           float ey = 0;//定义y坐标变量
           MapXLib.Points pts = new MapXLib.Points();

           ex = p1.X;
           ey = p1.Y;
           axMap1.ConvertCoord(ref ex, ref ey, ref MapX, ref MapY, MapXLib.ConversionConstants.miScreenToMap);
           pts.AddXY(MapX, MapY, Missing.Value);

           ex = p2.X;
           ey = p2.Y;
           axMap1.ConvertCoord(ref ex, ref ey, ref MapX, ref MapY, MapXLib.ConversionConstants.miScreenToMap);
           pts.AddXY(MapX, MapY, Missing.Value);

           tempFea = axMap1.FeatureFactory.CreateLine(pts, tempStyle);
           lyr.AddFeature(tempFea, Missing.Value);//添加矩形边框
           lyr.Refresh();

    }

        private void draw_line_with_mapx(PointF[] p, uint color, int line_width)
        {

            if (g_temp_layer == null)
                return;

            MapXLib.Layer lyr = g_temp_layer;// axMap1.Layers._Item(this.layer_name);

            MapXLib.Feature tempFea;//声明Feature变量
            MapXLib.Style tempStyle = new MapXLib.Style();//声明Style变量

            if (line_width > 6)
                line_width = 6;

            tempStyle.LineColor = (((color & 0x000000ff) << 16)) | ((((color & 0x0000ff00)))) | (((color & 0x00ff0000) >>16)) ;
            tempStyle.LineWidth = (short)line_width;//设置Style的矩形边框宽度

            double MapX = 0;//定义x坐标变量
            double MapY = 0;//定义y坐标变量
            float ex = 0;//定义x坐标变量
            float ey = 0;//定义y坐标变量
            MapXLib.Points pts = new MapXLib.Points();
            
            for (int i = 0; i < p.Length; i++)
            {
                ex = p[i].X;
                ey = p[i].Y;
                axMap1.ConvertCoord(ref ex, ref ey, ref MapX, ref MapY, MapXLib.ConversionConstants.miScreenToMap);
                pts.AddXY(MapX, MapY, Missing.Value);
            }
            tempFea = axMap1.FeatureFactory.CreateLine(pts, tempStyle);
            lyr.AddFeature(tempFea, Missing.Value);//添加矩形边框
            lyr.Refresh();

        }

        private void treeListGis_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {

        }
         int g_indent = 1;
        private void button_indent_Click(object sender, EventArgs e)
        {
            if (g_indent == 0)
            {
                g_list_wid = 300;
                resize_treelist(g_list_wid);
                g_indent = 1;
            }
            else
            {
                g_list_wid = 42;

                resize_treelist(g_list_wid);
                g_indent = 0;
            }
        }

        private void road_select_list_AfterCollapse(object sender, NodeEventArgs e)
        {
            e.Node.Expanded = true;
        }

        private void road_select_list_CustomNodeCellEdit(object sender, GetCustomNodeCellEditEventArgs e)
        {
            if (e.Column.FieldName == "路线名称")
            {
                e.RepositoryItem = repositoryItemTextEdit2;
            }
            else if (e.Column.FieldName == "归档日期")
            {
                DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox11 =
                 new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
                e.RepositoryItem = repositoryItemComboBox11;
               
                
                {

                    if (e.Node != null)
                    {
                        string pa = (string)e.Node.GetValue(0);

                        for (int i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                        {
                            if (gis_road_basic_information_unit_index[i].road_name.CompareTo(pa) == 0)
                            {
                                if (gis_road_basic_information_unit_index[i].archive_date != null)
                                {
                                    DateTime dts = new DateTime(2001, 12, 14);
                                    foreach (DateTime dt in gis_road_basic_information_unit_index[i].archive_date)
                                    {
                                        //repositoryItemComboBox11.Items.Add(dt.ToShortDateString());
                                        repositoryItemComboBox11.Items.Add(dt);;
                                        dts = dt;
                                        
                                    }

                                 //   e.Node.SetValue(1, dts);
                                }
                                break;
                            }
                        }
                        
                    }
                }

        

            }
            else if (e.Column.FieldName == "路幅")
            {
                DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2 =
                        new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
                e.RepositoryItem = repositoryItemComboBox2;
                repositoryItemComboBox2.Items.Add("上行");
                repositoryItemComboBox2.Items.Add("下行");
            }
            else if (e.Column.FieldName == "选择")
            {
                e.RepositoryItem = repositoryItemCheckEdit2;
            }
            return;
        }
        void init_road_select_list_data()
        {
            Record[] records = new Record[11];
            records[0] = new Record("Product Name", "Chai", "Teatime Chocolate Biscuits", "Ipoh Coffee", 0);
            records[1] = new Record("Category", 1, 2, 1, 1);
            records[2] = new Record("Supplier", "Exotic Liquids", "Specialty Biscuits, Ltd.", "Leka Trading", 2);
            records[3] = new Record("Quantity Per Unit", "10 boxes x 20 bags", "10 boxes x 12 pieces", "16 - 500 g tins", 3, 0);
            records[4] = new Record("Unit Price", 18.00, 9.20, 46.00, 4, 0);
            records[5] = new Record("Units in Stock", 39, 25, 17, 5, 0);
            records[6] = new Record("Discontinued", true, false, true, 6, 0);
            records[7] = new Record("Last Order", new DateTime(2001, 12, 14), new DateTime(2000, 7, 20), new DateTime(2002, 1, 7), 7);
            records[8] = new Record("Relevance", 70, 90, 50, 8);
            records[9] = new Record("Contact Name", "Shelley Burke", "Robb Merchant", "Sven Petersen", 9, 2);
            records[10] = new Record("Phone", "(100)555-4822", "(111)555-1222", "(120)555-1154", 10, 2);

            road_select_list.DataSource = records;
        }
         int g_ig = 0;
        private void button_indent2_onClick(object sender, EventArgs e)
        {

            if (g_ig == 0)
            {
            g_list_road_height = 250;
            g_ig = 1;
            }
            else
            {
                g_list_road_height = 0;
                g_ig = 0;
            }
            resize_treelist(g_list_wid); 
        }

        private void button_indent2_MouseCaptureChanged(object sender, EventArgs e)
        {

        }

        private void 显示基本信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // find the road info
            int select_road = 0;
            int i;
            {
                MapXLib.Layer layer;

                try
                {
                    layer = axMap1.Layers._Item(this.layer_name);
                }
                catch
                {
                    return;
                }
                if(layer.Selection.Count<=0)
                    return;
                int ccc = layer.Selection._Item(1).FeatureID;

                for ( i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                {
                    if (gis_road_basic_information_unit_index[i].road_feature_id == ccc)
                        break;
                }
                if (gis_road_basic_information_unit_index.Length == i) // can not get the feature id from the road
                {
                    return;
                }
                select_road = i;
            }

            double lat = gis_road_basic_information_unit_index[select_road].latitude_longitude[0];
            double lon = gis_road_basic_information_unit_index[select_road].latitude_longitude[1];
            string div = "---------------------------------------\n";
            string s_lat = "经度:" + lat + "\n" + div;
            string s_lon = "纬度:" + lon + "\n" + div;
            string s_name = "路面名称:" + gis_road_basic_information_unit_index[select_road].road_name + "\n" + div;
            string s_degree = "路面等级:" + gis_road_basic_information_unit_index[select_road].road_degree + "\n" + div;
            string s_channel = "通道:" + gis_road_basic_information_unit_index[select_road].road_channels + "\n" + div;
            MessageBox.Show(s_lat + s_lon + s_name + s_degree + s_channel, "路面基本信息：", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        private void googleEarthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int select_road = 0;
            int i;
            {
                MapXLib.Layer layer;

                try
                {
                    layer = axMap1.Layers._Item(this.layer_name);
                }
                catch
                {
                    return;
                }
                if (layer.Selection.Count <= 0)
                    return;
                int ccc = layer.Selection._Item(1).FeatureID;

                for (i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                {
                    if (gis_road_basic_information_unit_index[i].road_feature_id == ccc)
                        break;
                }
                if (gis_road_basic_information_unit_index.Length == i) // can not get the feature id from the road
                {
                    return;
                }
                select_road = i;
            }
            {
                //  settime(2000);
                change_to_google_earth();

                do_google_earth_act(treeListGis.FocusedNode.GetValue(0).ToString(), gis_road_basic_information_unit_index[select_road].latitude_longitude[0],
                    gis_road_basic_information_unit_index[i].latitude_longitude[1], gis_road_basic_information_unit_index[select_road].tip_link_url_pic,
                    gis_road_basic_information_unit_index[i].road_name, gis_road_basic_information_unit_index[select_road].tip_link_url);

          
            }
        }

        private void google_earth_Click(object sender, EventArgs e)
        {
            change_to_map();
           // google_earth
        }
        private void pop_option_menu_tip()
        {
            MessageBox.Show("请点击工具栏“选择”项，并点击地图选择一条路线", "提示：", MessageBoxButtons.OK, MessageBoxIcon.None);
        }
        private void cms_Opening(object sender, CancelEventArgs e)
        {
            int select_road = 0;
            bool ok = true;
            int i;
            {
                MapXLib.Layer layer;

                try
                {
                    layer = axMap1.Layers._Item(this.layer_name);
                }
                catch
                {
                    e.Cancel = true;
                    return;
                }
                if (layer.Selection.Count <= 0)
                {
                    
                    pop_option_menu_tip();
                    e.Cancel = true;
                    return;
                }
                int ccc = layer.Selection._Item(1).FeatureID;

                for (i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                {
                    if (gis_road_basic_information_unit_index[i].road_feature_id == ccc)
                        break;
                }
                if (gis_road_basic_information_unit_index.Length == i) // can not get the feature id from the road
                {
                        pop_option_menu_tip();
                        e.Cancel = true;
                        return;
                    
                }
              
            }
        }

        private void toolStripGoogleEarth_Click(object sender, EventArgs e)
        {
            int select_road = 0;
            int i;
            {
                MapXLib.Layer layer;

                try
                {
                    layer = axMap1.Layers._Item(this.layer_name);
                }
                catch
                {
                    return;
                }
                if (layer.Selection.Count <= 0)
                {
                    MessageBox.Show("请点击工具栏“选择”项，并点击地图选择一条路线", "提示：", MessageBoxButtons.OK, MessageBoxIcon.None);
                    return;
                }
                int ccc = layer.Selection._Item(1).FeatureID;

                for (i = 0; i < gis_road_basic_information_unit_index.Length; i++)
                {
                    if (gis_road_basic_information_unit_index[i].road_feature_id == ccc)
                        break;
                }
                if (gis_road_basic_information_unit_index.Length == i) // can not get the feature id from the road
                {
                    MessageBox.Show("请点击工具栏“选择”项，并点击地图选择一条路线", "提示：", MessageBoxButtons.OK, MessageBoxIcon.None);
                    return;
                }
                select_road = i;
            }
            {
                //  settime(2000);
                change_to_google_earth();

                do_google_earth_act(treeListGis.FocusedNode.GetValue(0).ToString(), gis_road_basic_information_unit_index[select_road].latitude_longitude[0],
                    gis_road_basic_information_unit_index[i].latitude_longitude[1], gis_road_basic_information_unit_index[select_road].tip_link_url_pic,
                    gis_road_basic_information_unit_index[i].road_name, gis_road_basic_information_unit_index[select_road].tip_link_url);


            }
        }



    }

    public class Record
    {
        private string fCategory;
        private object fProduct1;
        private object fProduct2;
        private object fProduct3;
        private int fId;
        private int fParentID;

        public Record(string category, object product1, object product2, object product3, int id) : this(category, product1, product2, product3, id, -1) { }
        public Record(string category, object product1, object product2, object product3, int id, int parentID)
        {
            this.fCategory = category;
            this.fProduct1 = product1;
            this.fProduct2 = product2;
            this.fProduct3 = product3;
            this.fId = id;
            this.fParentID = parentID;
        }

        public int ID
        {
            get { return fId; }
        }

        public int ParentID
        {
            get { return fParentID; }
        }

        public string 路线名称
        {
            get { return fCategory; }
        }

        public object 归档日期
        {
            get { return fProduct1; }
            set { fProduct1 = value; }
        }

        public object 路幅
        {
            get { return fProduct2; }
            set { fProduct2 = value; }
        }

        public object 选择
        {
            get { return fProduct3; }
            set { fProduct3 = value; }
        }
    }
}